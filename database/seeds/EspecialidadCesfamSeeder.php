<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class EspecialidadCesfamSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//Cesfam 1        
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '1', 
            'cesfam_id' => '1',
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '2', 
            'cesfam_id' => '1',
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '3', 
            'cesfam_id' => '1',
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '4', 
            'cesfam_id' => '1',
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '5', 
            'cesfam_id' => '1',
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '6', 
            'cesfam_id' => '1',
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '7', 
            'cesfam_id' => '1',
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '8', 
            'cesfam_id' => '1',
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '9', 
            'cesfam_id' => '1',
            'created_at' => Carbon::now('America/Santiago'),
        ]);

        //Cesfam 2
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '1', 
            'cesfam_id' => '2',
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '2', 
            'cesfam_id' => '2',
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '4', 
            'cesfam_id' => '2',
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '5', 
            'cesfam_id' => '2',
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Cesfam 3
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '1', 
            'cesfam_id' => '3',
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '4', 
            'cesfam_id' => '3',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('especialidad_cesfam')->insert([
            'cupo' => '9',
            'especialidad_id' => '5', 
            'cesfam_id' => '3',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
    }
}
