<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class FirmaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $length=16;
        for ($i = 0; $i < $length; $i++) {
            DB::table('firma')->insert([
                'ruta' => 'ruta'.($i+1),
                'created_at' => Carbon::now('America/Santiago'),
            ]);
        }
    }
}
