<?php

use Illuminate\Database\Seeder;

class PacienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        $pacienteFemenino1 = factory(App\Model\Paciente::class, 10)->states('femenino_cesfam1')->create();
        $pacienteMasculino2 = factory(App\Model\Paciente::class, 10)->states('masculino_cesfam1')->create();

        $pacienteFemenino3 = factory(App\Model\Paciente::class, 5)->states('femenino_cesfam2')->create();
        $pacienteMasculino4 = factory(App\Model\Paciente::class, 5)->states('masculino_cesfam2')->create();
        
        $pacienteFemenino5 = factory(App\Model\Paciente::class, 5)->states('femenino_cesfam3')->create();
        $pacienteMasculino6 = factory(App\Model\Paciente::class, 5)->states('masculino_cesfam3')->create();
    }
}
