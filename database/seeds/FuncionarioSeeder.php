<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class FuncionarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $funcionarioFemenino = factory(App\Model\Funcionario::class, 8)->states('femenino')->create();
        $funcionarioMasculino = factory(App\Model\Funcionario::class, 8)->states('masculino')->create();
    }
}
