<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class PacienteEnfermedadMedicamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paciente_enfermedad_medicamento')->insert([
            'indicacion' =>'1 pastilla c/u 8 horas',
            'medicamento_id' => 1,
            'paciente_enfermedad_id' =>1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('paciente_enfermedad_medicamento')->insert([
            'indicacion' =>'1 pastilla c/u 12 horas',
            'medicamento_id' => 2,
            'paciente_enfermedad_id' =>1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('paciente_enfermedad_medicamento')->insert([
            'indicacion' =>'1 pastilla al dia',
            'medicamento_id' => 9,
            'paciente_enfermedad_id' => 2,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('paciente_enfermedad_medicamento')->insert([
            'indicacion' =>'Inyección cada un dia',
            'medicamento_id' => 10,
            'paciente_enfermedad_id' => 2,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('paciente_enfermedad_medicamento')->insert([
            'indicacion' =>'Dos puff cada 10 horas',
            'medicamento_id' => 13,
            'paciente_enfermedad_id' =>3,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('paciente_enfermedad_medicamento')->insert([
            'indicacion' =>'1 pastilla cada vez que tenga picor en garganta o nariz',
            'medicamento_id' => 15,
            'paciente_enfermedad_id' =>4,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('paciente_enfermedad_medicamento')->insert([
            'indicacion' =>'1 pastilla al despertar y al dormir',
            'medicamento_id' => 22,
            'paciente_enfermedad_id' =>5,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('paciente_enfermedad_medicamento')->insert([
            'indicacion' =>'1 pastilla antes de dormir',
            'medicamento_id' => 24,
            'paciente_enfermedad_id' =>6,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
    }
}
