<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class MedicamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $medicamento = ['Amlodipino 5 mg','Atenolol 50mg','Carvedilol 25mg','Enalapril 10mg','Enalapril 20mg','Espironolactona 25mg','Atorvastatina 10mg'
        ,'Atorvastatina 20mg','Glibenclamida Comprimido 5 mg','Insulina Cristalizada Humana  Frasco Ampolla 10 ml','Insulina Isófana Humana (NPH) Frasco Ampolla 10 ml'
        ,'Metformina Clorhidrato Comprimido 850 mg','Salbutamol Sulfato Aerosol dosificador 200 dosis','Salmeterol Xinafoato Aerosol dosificador 120 dosis',
        'Clorfenamina Comprimido 4 mg','Desloratadina Comprimido 5 mg','Loratadina Comprimido 10 mg','Prednisona Comprimido 5 mg','Prednisona Comprimido 20 mg',
        'Amitriptilina Clorhidrato Comprimido 25 mg','Imipramina Clorhidrato Comprimido 25 mg','Sertralina Comprimido 50 mg','Alprazolam Comprimido 0,5 mg','Diazepam Comprimido 10 mg',
    'Diazepam Ampolla 2 ml'];
        
        $length = count($medicamento);
        for ($i = 0; $i < $length; $i++) {
            DB::table('medicamento')->insert([
                'nombre_medicamento' => $medicamento[$i],
                'created_at' => Carbon::now('America/Santiago'),
            ]);
        }
    }
}
