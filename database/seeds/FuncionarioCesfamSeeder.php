<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class FuncionarioCesfamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //Cesfam 1
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 1,
            'cesfam_id' =>1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
               
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 2,
            'cesfam_id' =>1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 3,
            'cesfam_id' =>1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 4,
            'cesfam_id' =>1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 5,
            'cesfam_id' =>1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 6,
            'cesfam_id' =>1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 7,
            'cesfam_id' =>1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 8,
            'cesfam_id' =>1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 9,
            'cesfam_id' =>1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 10,
            'cesfam_id' => 1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);   
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 11,
            'cesfam_id' =>1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);

        //Cesfam 2
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 1,
            'cesfam_id' =>2,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 12,
            'cesfam_id' =>2,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 13,
            'cesfam_id' =>2,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 14,
            'cesfam_id' =>2,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Cesfam 3
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 1,
            'cesfam_id' =>3,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 15,
            'cesfam_id' =>3,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('funcionario_cesfam')->insert([
            'funcionario_id' => 16,
            'cesfam_id' =>3,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
    }
}
