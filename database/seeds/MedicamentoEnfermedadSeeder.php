<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class MedicamentoEnfermedadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 1,
            'medicamento_id' =>1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 1,
            'medicamento_id' =>2,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 1,
            'medicamento_id' =>3,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 1,
            'medicamento_id' =>4,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 1,
            'medicamento_id' =>5,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 1,
            'medicamento_id' =>6,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 2,
            'medicamento_id' =>7,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 2,
            'medicamento_id' =>8,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 3,
            'medicamento_id' =>9,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 3,
            'medicamento_id' =>10,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 3,
            'medicamento_id' =>11,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 3,
            'medicamento_id' =>12,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 4,
            'medicamento_id' =>13,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 4,
            'medicamento_id' =>14,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Anafilaxia
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 5,
            'medicamento_id' =>15,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 5,
            'medicamento_id' =>16,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 5,
            'medicamento_id' =>17,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 5,
            'medicamento_id' =>18,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 5,
            'medicamento_id' =>19,
            'created_at' => Carbon::now('America/Santiago'),
        ]);

        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 6,
            'medicamento_id' =>15,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 6,
            'medicamento_id' =>16,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 6,
            'medicamento_id' =>17,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 7,
            'medicamento_id' =>20,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 7,
            'medicamento_id' =>21,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 7,
            'medicamento_id' =>22,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 7,
            'medicamento_id' =>24,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 8,
            'medicamento_id' =>23,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 8,
            'medicamento_id' =>24,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('enfermedad_medicamento')->insert([
            'enfermedad_id' => 9,
            'medicamento_id' =>25,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
    }
}
