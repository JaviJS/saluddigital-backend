<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class EspecialidadFuncionarioCesfamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $especialidad = ['Médico General','Pediatría','Kinesiología','Enfermería','Nutricion','Radiología','Oftalmológica','Ecografía','Muestra de Sangre'];
        //Funcionario 1, Cesfam 1, Medico General
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 1, 
            'especialidad_cesfam_id' => 1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 2, Cesfam 1, Pediatría
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 2, 
            'especialidad_cesfam_id' => 2,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 3, Cesfam 1, Kinesiologia
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 3, 
            'especialidad_cesfam_id' => 3,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 4, Cesfam 1, Enfermería
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 4, 
            'especialidad_cesfam_id' => 4,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 4, Cesfam 1, Muestra de sangre
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 4, 
            'especialidad_cesfam_id' => 9,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 5, Cesfam 1, Nutricion
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 5, 
            'especialidad_cesfam_id' => 5,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 6, Cesfam 1, Radiología
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 6, 
            'especialidad_cesfam_id' => 6,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 7, Cesfam 1, Oftalmológica
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 7, 
            'especialidad_cesfam_id' => 7,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 8, Cesfam 1, Ecografía
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 8, 
            'especialidad_cesfam_id' => 8,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 9, Cesfam 1, Muestra de Sangre
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 9, 
            'especialidad_cesfam_id' => 9,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
         //Funcionario 9, Cesfam 1, Enfermeria
         DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 9, 
            'especialidad_cesfam_id' => 4,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 10, Cesfam 1, Médico General
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 10, 
            'especialidad_cesfam_id' => 1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 11, Cesfam 1, Médico General
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 11, 
            'especialidad_cesfam_id' => 1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 1, Cesfam 2, Medico General
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 12, 
            'especialidad_cesfam_id' => 10,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 12, Cesfam 2, Pediatría
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 13, 
            'especialidad_cesfam_id' => 11,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 13, Cesfam 2, Enfermería
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 14, 
            'especialidad_cesfam_id' => 12,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 14, Cesfam 2, Nutrición
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 15, 
            'especialidad_cesfam_id' => 13,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 1, Cesfam 3, Médico General
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 16, 
            'especialidad_cesfam_id' => 14,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 15, Cesfam 3, Enfermería
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 17, 
            'especialidad_cesfam_id' => 15,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        //Funcionario 16, Cesfam 3, Pediatría
        DB::table('especialidad_funcionario_cesfam')->insert([
            'funcionario_cesfam_id' => 18, 
            'especialidad_cesfam_id' => 16,
            'created_at' => Carbon::now('America/Santiago'),
        ]);

    }
}
