<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class PacienteEnfermedadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //9 enfermedades
       
    
        DB::table('paciente_enfermedad')->insert([
            'fecha_inicio' =>'2018-08-25',
            'paciente_id' => 1,
            'enfermedad_id' =>1,
            'created_at' => Carbon::now('America/Santiago'),
        ]);

        DB::table('paciente_enfermedad')->insert([
            'fecha_inicio' =>'2017-06-25',
            'paciente_id' => 1,
            'enfermedad_id' =>2,
            'created_at' => Carbon::now('America/Santiago'),
        ]);

        DB::table('paciente_enfermedad')->insert([
            'fecha_inicio' =>'2020-03-25',
            'paciente_id' => 3,
            'enfermedad_id' =>4,
            'created_at' => Carbon::now('America/Santiago'),
        ]);

        DB::table('paciente_enfermedad')->insert([
            'fecha_inicio' =>'2015-01-20',
            'paciente_id' => 5,
            'enfermedad_id' =>6,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('paciente_enfermedad')->insert([
            'fecha_inicio' =>'2016-06-25',
            'paciente_id' => 6,
            'enfermedad_id' =>7,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
        DB::table('paciente_enfermedad')->insert([
            'fecha_inicio' =>'2015-06-01',
            'paciente_id' => 6,
            'enfermedad_id' =>8,
            'created_at' => Carbon::now('America/Santiago'),
        ]);
    }
}
