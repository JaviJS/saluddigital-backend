<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class SalaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('sala')->insert([
            'nombre_sala' => '101',
            'nombre_pabellon' => 'Pabellón A', 
            'especialidad_cesfam_id' => '1',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('sala')->insert([
            'nombre_sala' => '102',
            'nombre_pabellon' => 'Pabellón A', 
            'especialidad_cesfam_id' => '2',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('sala')->insert([
            'nombre_sala' => '103',
            'nombre_pabellon' => 'Pabellón A', 
            'especialidad_cesfam_id' => '3',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('sala')->insert([
            'nombre_sala' => '104',
            'nombre_pabellon' => 'Pabellón B', 
            'especialidad_cesfam_id' => '4',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('sala')->insert([
            'nombre_sala' => '105',
            'nombre_pabellon' => 'Pabellón B', 
            'especialidad_cesfam_id' => '5',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('sala')->insert([
            'nombre_sala' => '106',
            'nombre_pabellon' => 'Pabellón C', 
            'especialidad_cesfam_id' => '6',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('sala')->insert([
            'nombre_sala' => '107',
            'nombre_pabellon' => 'Pabellón C', 
            'especialidad_cesfam_id' => '7',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('sala')->insert([
            'nombre_sala' => '108',
            'nombre_pabellon' => 'Pabellón C', 
            'especialidad_cesfam_id' => '8',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('sala')->insert([
            'nombre_sala' => '110',
            'nombre_pabellon' => 'Pabellón C', 
            'especialidad_cesfam_id' => '9',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('sala')->insert([
            'nombre_sala' => 'Verde 201',
            'nombre_pabellon' => 'Pabellón General', 
            'especialidad_cesfam_id' => '10',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('sala')->insert([
            'nombre_sala' => 'Verde 211',
            'nombre_pabellon' => 'Pabellón General', 
            'especialidad_cesfam_id' => '11',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('sala')->insert([
            'nombre_sala' => 'Amarillo 215',
            'nombre_pabellon' => 'Pabellón General', 
            'especialidad_cesfam_id' => '12',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('sala')->insert([
            'nombre_sala' => 'Rojo 302',
            'nombre_pabellon' => 'Pabellón General', 
            'especialidad_cesfam_id' => '13',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('sala')->insert([
            'nombre_sala' => '11',
            'nombre_pabellon' => 'Pabellón Sur', 
            'especialidad_cesfam_id' => '14',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('sala')->insert([
            'nombre_sala' => '10',
            'nombre_pabellon' => 'Pabellón Norte', 
            'especialidad_cesfam_id' => '15',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
        DB::table('sala')->insert([
            'nombre_sala' => '12',
            'nombre_pabellon' => 'Pabellón Centro', 
            'especialidad_cesfam_id' => '16',
            'created_at' => Carbon::now('America/Santiago'),
        ]);  
 
    }
}
