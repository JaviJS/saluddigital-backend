<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class EspecialidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $especialidad = ['Médico General','Pediatría','Kinesiología','Enfermería','Nutricion','Radiología','Oftalmológica','Ecografía','Muestra de Sangre'];
        $length = count($especialidad);
        for ($i = 0; $i < $length; $i++) {
            DB::table('especialidad')->insert([
                'nombre_especialidad' => $especialidad[$i],
                'created_at' => Carbon::now('America/Santiago'),
            ]);
        }
    }
}
