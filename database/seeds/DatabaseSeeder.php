<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(UserSeeder::class);
        $this->call(EspecialidadSeeder::class);
        $this->call(CesfamSeeder::class);
        $this->call(EspecialidadCesfamSeeder::class);
        $this->call(SalaSeeder::class);
        $this->call(FirmaSeeder::class);
        $this->call(FuncionarioSeeder::class);
        $this->call(PacienteSeeder::class);
        $this->call(EnfermedadSeeder::class);
        $this->call(MedicamentoSeeder::class);
        $this->call(MedicamentoEnfermedadSeeder::class);
        $this->call(PacienteEnfermedadSeeder::class);
        $this->call(PacienteEnfermedadMedicamentoSeeder::class);
        $this->call(FuncionarioCesfamSeeder::class);
        $this->call(EspecialidadFuncionarioCesfamSeeder::class);
        $this->call(CitaMedicaSeeder::class);
        $this->call(TurnoSeeder::class);
    }
}
