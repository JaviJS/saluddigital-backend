<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class EnfermedadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $enfermedad = ['Hipertensión','Diabetes','Dislipidemia','Asma','Anafilaxia','Alergia','Depresion','Transtorno de Sueño','Trastornos psiquiátricos'];
        $length = count($enfermedad);
        for ($i = 0; $i < $length; $i++) {
            DB::table('enfermedad')->insert([
                'nombre_enfermedad' => $enfermedad[$i],
                'created_at' => Carbon::now('America/Santiago'),
            ]);
        }
    }
}
