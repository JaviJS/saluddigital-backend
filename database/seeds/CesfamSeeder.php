<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class CesfamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
  

        $cesfam = ['Cesfam Amanecer','Cesfam Pueblo Nuevo','Cesfam Santa Rosa'];
        $direccion = ['Garibaldi 1280','Nahuelbuta 2815','Av. Pircunche 0316'];
        $telefono = ['2557300','2220038','2220194'];
        $length = count($cesfam);
        for ($i = 0; $i < $length; $i++) {
            DB::table('cesfam')->insert([
                'nombre_cesfam' => $cesfam[$i],
                'dirección' => $direccion[$i], 
                'comuna' => 'Temuco',
                'ciudad' => 'Temuco',
                'region' => 'Araucanía', 
                'telefono' => '+56 45 '.$telefono[$i], 
                'created_at' => Carbon::now('America/Santiago'),
            ]);
        }
    }
}
