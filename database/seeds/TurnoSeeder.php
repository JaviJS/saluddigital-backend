<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class TurnoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Horas turno inicio
        $turnoCompletoInicio=8;
        $turnoMañanaInicio=8;
        $turnoTardeInicio=[12,0];
        //Horas turno final
        $turnoCompletoFinal=17;
        $turnoMañanaFinal=[12,0];
        $turnoTardeFinal=17;

        //Arreglo con turnos de funcionarios, opcion 1
        $turnoDiaInicio1= [$turnoCompletoInicio,$turnoCompletoInicio,$turnoCompletoInicio,$turnoMañanaInicio,$turnoTardeInicio,$turnoCompletoInicio,$turnoCompletoInicio,
        $turnoCompletoInicio,$turnoCompletoInicio,$turnoMañanaInicio,$turnoTardeInicio,$turnoMañanaInicio,$turnoTardeInicio,$turnoMañanaInicio,$turnoCompletoInicio,
        $turnoCompletoInicio,$turnoCompletoInicio,$turnoTardeInicio,$turnoCompletoInicio,$turnoCompletoInicio];

        $turnoDiaFinal1= [$turnoCompletoFinal,$turnoCompletoFinal,$turnoCompletoFinal,$turnoMañanaFinal,$turnoTardeFinal,$turnoCompletoFinal,$turnoCompletoFinal,
        $turnoCompletoFinal,$turnoCompletoFinal,$turnoMañanaFinal,$turnoTardeFinal,$turnoMañanaFinal,$turnoTardeFinal,$turnoMañanaFinal,$turnoCompletoFinal,
        $turnoCompletoFinal,$turnoCompletoFinal,$turnoTardeFinal,$turnoCompletoFinal,$turnoCompletoFinal];

        //Arreglo con turnos de funcionarios, opcion 2
        $turnoDiaInicio2= [$turnoCompletoInicio,$turnoCompletoInicio,$turnoCompletoInicio,$turnoTardeInicio,$turnoMañanaInicio,$turnoCompletoInicio,$turnoCompletoInicio,
        $turnoCompletoInicio,$turnoCompletoInicio,$turnoTardeInicio, $turnoMañanaInicio,$turnoTardeInicio,$turnoMañanaInicio, $turnoTardeInicio, $turnoCompletoInicio,
        $turnoCompletoInicio,$turnoCompletoInicio,$turnoMañanaInicio,$turnoCompletoInicio,$turnoCompletoInicio];

        $turnoDiaFinal2= [$turnoCompletoFinal,$turnoCompletoFinal,$turnoCompletoFinal,$turnoTardeFinal,$turnoMañanaFinal,$turnoCompletoFinal,$turnoCompletoFinal,
        $turnoCompletoFinal,$turnoCompletoFinal,$turnoTardeFinal, $turnoMañanaFinal,$turnoTardeFinal,$turnoMañanaFinal, $turnoTardeFinal, $turnoCompletoFinal,
        $turnoCompletoFinal,$turnoCompletoFinal,$turnoMañanaFinal,$turnoCompletoFinal,$turnoCompletoFinal];

      //datos para semana 1 de funcionarios
        $cantFuncionarios=count($turnoDiaInicio1);
        $dia_inicio=Carbon::tomorrow('America/Santiago');  
        $cantDias=6;
        $salas=[1,2,3,4,9,5,6,7,8,9,4,1,1,10,11,12,13,14,15,16];
        for ($j = 0; $j <$cantDias; $j++) {
            for ($i = 0; $i < $cantFuncionarios; $i++) {
                if($turnoDiaInicio1[$i]!=$turnoTardeInicio){
                    $fecha_inicio= Carbon::create($dia_inicio->year, $dia_inicio->month, $dia_inicio->day, $turnoDiaInicio1[$i], 0,'America/Santiago');       
                }else{                
                    $fecha_inicio= Carbon::create($dia_inicio->year, $dia_inicio->month, $dia_inicio->day, $turnoDiaInicio1[$i][0], $turnoDiaInicio1[$i][1],'America/Santiago');
                }
    
                if($turnoDiaFinal1[$i]!=$turnoMañanaFinal){
                    $fecha_final= Carbon::create($dia_inicio->year, $dia_inicio->month, $dia_inicio->day, $turnoDiaFinal1[$i], 0,'America/Santiago');
                }else{
                    $fecha_final= Carbon::create($dia_inicio->year, $dia_inicio->month, $dia_inicio->day, $turnoDiaFinal1[$i][0], $turnoDiaFinal1[$i][1],'America/Santiago');
                }
                DB::table('turno')->insert([
                    'fecha_inicio' => $fecha_inicio, 
                    'fecha_fin' => $fecha_final,
                    'especialidad_funcionario_cesfam_id' => $i+1,
                    'sala_id' =>$salas[$i],
                    'created_at' => Carbon::now('America/Santiago'),
                ]);
            }            
            $dia_inicio= $dia_inicio->addDay(1);
        }

        //datos para semana 2 de funcionarios

        for ($j = 0; $j <$cantDias; $j++) {
            for ($i = 0; $i < $cantFuncionarios; $i++) {
                if($turnoDiaInicio2[$i]!=$turnoTardeInicio){
                    $fecha_inicio= Carbon::create($dia_inicio->year, $dia_inicio->month, $dia_inicio->day, $turnoDiaInicio2[$i], 0,'America/Santiago');       
                }else{                
                    $fecha_inicio= Carbon::create($dia_inicio->year, $dia_inicio->month, $dia_inicio->day, $turnoDiaInicio2[$i][0], $turnoDiaInicio2[$i][1],'America/Santiago');
                }
    
                if($turnoDiaFinal2[$i]!=$turnoMañanaFinal){
                    $fecha_final= Carbon::create($dia_inicio->year, $dia_inicio->month, $dia_inicio->day, $turnoDiaFinal2[$i], 0,'America/Santiago');
                }else{
                    $fecha_final= Carbon::create($dia_inicio->year, $dia_inicio->month, $dia_inicio->day, $turnoDiaFinal2[$i][0], $turnoDiaFinal2[$i][1],'America/Santiago');
                }
                DB::table('turno')->insert([
                    'fecha_inicio' => $fecha_inicio, 
                    'fecha_fin' => $fecha_final,
                    'especialidad_funcionario_cesfam_id' => $i+1,
                    'sala_id' =>$salas[$i],
                    'created_at' => Carbon::now('America/Santiago'),
                ]);
            }            
            $dia_inicio= $dia_inicio->addDay(1);
        } 
    }
}
