<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class CitaMedicaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    //Horas
    $hora_inicio=[8,9,10,11,12,13,14,15,16,17];
    $hora_final=[9,10,11,12,13,14,15,16,17,18];
    //cantidad de cupo
    $cupo=9;


    //Cesfam 1
    $especialidad_cesfam1=[1,2,3,4,5,6,7,8,9];
    $sala_cesfam1=[1,2,3,4,5,6,7,8,9];  
    $idPaciente=1;
    $cant_pacientes_cesfam1=20;
    $dia_inicio=Carbon::tomorrow('America/Santiago');
    $nro_espera=1;

    for ($j = 0; $j <count($especialidad_cesfam1); $j++) {
        for ($i = 0; $i < $cant_pacientes_cesfam1; $i++) {
            $fecha_inicio= Carbon::create($dia_inicio->year, $dia_inicio->month, $dia_inicio->day, $hora_inicio[$nro_espera-1], 0,'America/Santiago');
            $fecha_final= Carbon::create($dia_inicio->year, $dia_inicio->month, $dia_inicio->day, $hora_final[$nro_espera-1], 0,'America/Santiago');
     
            DB::table('cita_medica')->insert([
                'nro_espera' => $nro_espera,
                'fecha_inicio' => $fecha_inicio, 
                'fecha_final' => $fecha_final,
                'estado' => 'Pendiente',
                'especialidad_cesfam_id' => $especialidad_cesfam1[$j],
                'sala_id' =>$sala_cesfam1[$j],
                'paciente_id' => $idPaciente++,
                'created_at' => Carbon::now('America/Santiago'),
            ]);  
            
            if($nro_espera==$cupo){
                $dia_inicio= $dia_inicio->addDay(1);
                $nro_espera=1;
            }else{
                $nro_espera++;
            }       
        }
        $nro_espera=1;
        $idPaciente=1;
        $dia_inicio=Carbon::tomorrow('America/Santiago');
    }

    //Cesfam 2
    $especialidad_cesfam2=[10,11,12,13];
    $sala_cesfam2=[10,11,12,13];
    $cant_pacientes_cesfam2=10;
    $dia_inicio=Carbon::tomorrow('America/Santiago');
    $nro_espera=1;
    $idPaciente=21;
    for ($j = 0; $j <count($especialidad_cesfam2); $j++) {
        for ($i = 0; $i < $cant_pacientes_cesfam2; $i++) {
            $fecha_inicio= Carbon::create($dia_inicio->year, $dia_inicio->month, $dia_inicio->day, $hora_inicio[$nro_espera-1], 0,'America/Santiago');
            $fecha_final= Carbon::create($dia_inicio->year, $dia_inicio->month, $dia_inicio->day, $hora_final[$nro_espera-1], 0,'America/Santiago');
    
            DB::table('cita_medica')->insert([
                'nro_espera' => $nro_espera,
                'fecha_inicio' => $fecha_inicio, 
                'fecha_final' => $fecha_final,
                'estado' => 'Pendiente',
                'especialidad_cesfam_id' => $especialidad_cesfam2[$j],
                'sala_id' =>$sala_cesfam2[$j],
                'paciente_id' => $idPaciente++,
                'created_at' => Carbon::now('America/Santiago'),
            ]);  
            
            if($nro_espera==$cupo){
                $dia_inicio= $dia_inicio->addDay(1);
                $nro_espera=1;
            }else{
                $nro_espera++;
            }       
        }
        $nro_espera=1;
        $idPaciente=21;
        $dia_inicio=Carbon::tomorrow('America/Santiago');
    }


   //Cesfam 3
    $especialidad_cesfam3=[14,15,16];  
    $sala_cesfam3=[14,15,16];
    $cant_pacientes_cesfam3=10;
    $dia_inicio=Carbon::tomorrow('America/Santiago');
    $nro_espera=1;
    $idPaciente=31;
    for ($j = 0; $j <count($especialidad_cesfam3); $j++) {
        for ($i = 0; $i < $cant_pacientes_cesfam3; $i++) {
            $fecha_inicio= Carbon::create($dia_inicio->year, $dia_inicio->month, $dia_inicio->day, $hora_inicio[$nro_espera-1], 0,'America/Santiago');
            $fecha_final= Carbon::create($dia_inicio->year, $dia_inicio->month, $dia_inicio->day, $hora_final[$nro_espera-1], 0,'America/Santiago');
    
            DB::table('cita_medica')->insert([
                'nro_espera' => $nro_espera,
                'fecha_inicio' => $fecha_inicio, 
                'fecha_final' => $fecha_final,
                'estado' => 'Pendiente',
                'especialidad_cesfam_id' => $especialidad_cesfam3[$j],
                'sala_id' =>$sala_cesfam3[$j],
                'paciente_id' => $idPaciente++,
                'created_at' => Carbon::now('America/Santiago'),
            ]);  
            
            if($nro_espera==$cupo){
                $dia_inicio= $dia_inicio->addDay(1);
                $nro_espera=1;
            }else{
                $nro_espera++;
            }       
        }
        $nro_espera=1;
        $idPaciente=31;
        $dia_inicio=Carbon::tomorrow('America/Santiago');
    }
    }
}
