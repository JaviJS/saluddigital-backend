<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userFuncionario = factory(App\Model\User::class, 16)->states('funcionario')->create();
        $userPaciente = factory(App\Model\User::class, 40)->states('paciente')->create();
    }
}

