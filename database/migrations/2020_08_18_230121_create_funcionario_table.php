<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFuncionarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funcionario', function (Blueprint $table) {
            $table->id('id_funcionario');
            $table->string('nombres');
            $table->string('apellidos');
            $table->enum('titulo', ['Médica General','Médica Pediatra','Kinesióloga','Enfermera','Nutricionista','Tecnóloga Médica','Médica Oftalmóloga','Enfermero','Médico General','Médico Pedriatra' ]);
            $table->enum('sexo', ['Masculino', 'Femenino']);            
            $table->bigInteger('firma_id')->unsigned();
            $table->foreign('firma_id')->references('id_firma')->on('firma');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funcionario');
    }
}
