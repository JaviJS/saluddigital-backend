<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnfermedadMedicamentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enfermedad_medicamento', function (Blueprint $table) {
            $table->id('id_enfermedad_medicamento');
            $table->bigInteger('enfermedad_id')->unsigned()->required();
            $table->bigInteger('medicamento_id')->unsigned()->required();
            $table->foreign('enfermedad_id')->references('id_enfermedad')->on('enfermedad');
            $table->foreign('medicamento_id')->references('id_medicamento')->on('medicamento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enfermedad_medicamento');
    }
}
