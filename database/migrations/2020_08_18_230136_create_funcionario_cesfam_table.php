<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFuncionarioCesfamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funcionario_cesfam', function (Blueprint $table) {
            $table->id('id_funcionario_cesfam');            
            $table->bigInteger('funcionario_id')->unsigned()->required();
            $table->bigInteger('cesfam_id')->unsigned()->required();
            $table->foreign('funcionario_id')->references('id_funcionario')->on('funcionario');
            $table->foreign('cesfam_id')->references('id_cesfam')->on('cesfam');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funcionario_cesfam');
    }
}
