<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspecialidadFuncionarioCesfamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('especialidad_funcionario_cesfam', function (Blueprint $table) {
            $table->id('id_especialidad_funcionario_cesfam');
            $table->bigInteger('funcionario_cesfam_id')->unsigned()->required();
            $table->bigInteger('especialidad_cesfam_id')->unsigned()->required();
            $table->foreign('funcionario_cesfam_id')->references('id_funcionario_cesfam')->on('funcionario_cesfam');
            $table->foreign('especialidad_cesfam_id')->references('id_especialidad_cesfam')->on('especialidad_cesfam');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('especialidad_funcionario_cesfam');
    }
}
