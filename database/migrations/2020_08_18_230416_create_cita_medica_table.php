<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitaMedicaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cita_medica', function (Blueprint $table) {
            $table->id('id_cita_medica');
            $table->integer('nro_espera');
            $table->dateTime('fecha_inicio');
            $table->dateTime('fecha_final');
            $table->string('observacion')->nullable();
            $table->enum('estado', ['Pendiente', 'Finalizada','Cancelada']);  
            $table->bigInteger('especialidad_cesfam_id')->unsigned()->required();            
            $table->foreign('especialidad_cesfam_id')->references('id_especialidad_cesfam')->on('especialidad_cesfam');
            $table->bigInteger('sala_id')->unsigned()->required();
            $table->foreign('sala_id')->references('id_sala')->on('sala');
            $table->bigInteger('paciente_id')->unsigned()->required();
            $table->foreign('paciente_id')->references('id_paciente')->on('paciente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cita_medica');
    }
}
