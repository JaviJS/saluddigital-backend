<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacienteEnfermedadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente_enfermedad', function (Blueprint $table) {
            $table->id('id_paciente_enfermedad');
            $table->date('fecha_inicio');
            $table->bigInteger('paciente_id')->unsigned()->required();
            $table->bigInteger('enfermedad_id')->unsigned()->required();
            $table->foreign('paciente_id')->references('id_paciente')->on('paciente');
            $table->foreign('enfermedad_id')->references('id_enfermedad')->on('enfermedad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paciente_enfermedad');
    }
}
