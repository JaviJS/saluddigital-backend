<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspecialidadCesfamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('especialidad_cesfam', function (Blueprint $table) {
            $table->id('id_especialidad_cesfam');
            $table->integer('cupo');
            $table->bigInteger('especialidad_id')->unsigned()->required();
            $table->bigInteger('cesfam_id')->unsigned()->required();
            $table->foreign('especialidad_id')->references('id_especialidad')->on('especialidad');
            $table->foreign('cesfam_id')->references('id_cesfam')->on('cesfam');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('especialidad_cesfam');
    }
}
