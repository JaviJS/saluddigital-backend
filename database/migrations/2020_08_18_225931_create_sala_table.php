<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sala', function (Blueprint $table) {
            $table->id('id_sala');
            $table->string('nombre_sala');
            $table->string('nombre_pabellon');
            $table->bigInteger('especialidad_cesfam_id')->unsigned()->required();
            $table->foreign('especialidad_cesfam_id')->references('id_especialidad_cesfam')->on('especialidad_cesfam');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sala');
    }
}
