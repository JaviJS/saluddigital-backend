<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente', function (Blueprint $table) {
            $table->id('id_paciente');
            $table->string('nombres');
            $table->string('apellidos');
            $table->date('fecha_nacimiento');
            $table->integer('edad');
            $table->enum('sexo', ['Masculino', 'Femenino']);            
            $table->string('nacionalidad');            
            $table->string('lugar_nacimiento');      
            $table->string('direccion');          
            $table->string('telefono_celular');
            $table->string('telefono_casa');
            $table->enum('sistema_salud', ['FONASA', 'Isapre']);  
            $table->enum('grupo_sanguineo', ['A+','A-', 'B+','B-','AB+','AB-','O+','O-']); 
            $table->bigInteger('cesfam_id')->unsigned()->required();
            $table->foreign('cesfam_id')->references('id_cesfam')->on('cesfam');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paciente');
    }
}
