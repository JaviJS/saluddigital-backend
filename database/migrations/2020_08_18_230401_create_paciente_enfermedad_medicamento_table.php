<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacienteEnfermedadMedicamentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente_enfermedad_medicamento', function (Blueprint $table) {
            $table->id('id_paciente_enfermedad_medicamento');
            $table->string('indicacion');
            $table->bigInteger('medicamento_id')->unsigned()->required();
            $table->bigInteger('paciente_enfermedad_id')->unsigned()->required();
            $table->foreign('medicamento_id')->references('id_medicamento')->on('medicamento');
            $table->foreign('paciente_enfermedad_id')->references('id_paciente_enfermedad')->on('paciente_enfermedad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paciente_enfermedad_medicamento');
    }
}
