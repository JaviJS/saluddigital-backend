<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCesfamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cesfam', function (Blueprint $table) {
            $table->id('id_cesfam');
            $table->string('nombre_cesfam');
            $table->string('dirección');
            $table->string('comuna');
            $table->string('ciudad');
            $table->string('region');
            $table->string('telefono');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cesfam');
    }
}
