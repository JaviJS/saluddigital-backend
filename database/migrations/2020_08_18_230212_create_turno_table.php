<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTurnoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turno', function (Blueprint $table) {
            $table->id('id_turno');
            $table->dateTime('fecha_inicio');
            $table->dateTime('fecha_fin');
            $table->bigInteger('especialidad_funcionario_cesfam_id')->unsigned()->required();
            $table->bigInteger('sala_id')->unsigned()->required();
            $table->foreign('especialidad_funcionario_cesfam_id')->references('id_especialidad_funcionario_cesfam')->on('especialidad_funcionario_cesfam');
            $table->foreign('sala_id')->references('id_sala')->on('sala');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turno');
    }
}
