<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Paciente;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Paciente::class, function (Faker $faker) {   
    static $id_user=17;
    $fecha_nacimiento= $faker-> date($format = 'Y-m-d', $max = 'now');
    return [
        'nombres' => $faker->firstNameMale .' '. $faker->firstNameMale,
        'apellidos' => $faker->lastName .' '. $faker->lastName,
        'fecha_nacimiento' => $fecha_nacimiento,
        'edad' => Carbon::createFromDate($fecha_nacimiento)->age,
        'sexo' => $faker->randomElement($array = array ('Masculino','Femenino')),
        'nacionalidad' => $faker->randomElement($array = array ('Chilena','Argentina','Paraguaya','Mexicana','Venezolana')),
        'lugar_nacimiento' => $faker->cityPrefix . ' ' . $faker->country,
        'direccion' => $faker->streetAddress . ', ' . $faker->cityPrefix . ' ' . $faker->country,
        'telefono_celular' => '+56 9'.$faker->unique()->randomNumber($nbDigits = 8,$strict = true),
        'telefono_casa' => '+45 2'.$faker->unique()->randomNumber($nbDigits = 6,$strict = true),
        'sistema_salud' => $faker->randomElement($array = array ('FONASA', 'Isapre')),
        'grupo_sanguineo' =>  $faker->randomElement($array = array ('A+','A-', 'B+','B-','AB+','AB-','O+','O-')),
        'cesfam_id' => $faker->numberBetween($min = 1, $max = 3),
        'user_id' => $id_user++,
    ];
});

$factory->state(Paciente::class, 'femenino_cesfam1', function (Faker $faker) {
    static $id_user=17;
    $fecha_nacimiento= $faker-> date($format = 'Y-m-d', $max = 'now');
    return [
        'nombres' => $faker->firstNameFemale .' '. $faker->firstNameFemale,
        'apellidos' => $faker->lastName .' '. $faker->lastName,
        'fecha_nacimiento' => $fecha_nacimiento,
        'edad' => Carbon::createFromDate($fecha_nacimiento)->age,
        'sexo' => 'Femenino',
        'nacionalidad' => $faker->randomElement($array = array ('Chilena','Argentina','Paraguaya','Mexicana','Venezolana')),
        'lugar_nacimiento' => 'Temuco, Chile',
        'direccion' => $faker->streetAddress . ', ' .'Temuco, Chile',
        'telefono_celular' => '+56 9'.$faker->unique()->randomNumber($nbDigits = 8,$strict = true),
        'telefono_casa' => '+45 2'.$faker->unique()->randomNumber($nbDigits = 6,$strict = true),
        'sistema_salud' => $faker->randomElement($array = array ('FONASA', 'Isapre')),
        'grupo_sanguineo' =>  $faker->randomElement($array = array ('A+','A-', 'B+','B-','AB+','AB-','O+','O-')),
        'cesfam_id' => 1,
        'user_id' => $id_user++,
    ];
  });
  $factory->state(Paciente::class, 'femenino_cesfam2', function (Faker $faker) {
    static $id_user=37;
    $fecha_nacimiento= $faker-> date($format = 'Y-m-d', $max = 'now');
    return [
        'nombres' => $faker->firstNameFemale .' '. $faker->firstNameFemale,
        'apellidos' => $faker->lastName .' '. $faker->lastName,
        'fecha_nacimiento' => $fecha_nacimiento,
        'edad' => Carbon::createFromDate($fecha_nacimiento)->age,
        'sexo' => 'Femenino',
        'nacionalidad' => $faker->randomElement($array = array ('Chilena','Argentina','Paraguaya','Mexicana','Venezolana')),
        'lugar_nacimiento' => 'Temuco, Chile',
        'direccion' => $faker->streetAddress . ', ' .'Temuco, Chile',
        'telefono_celular' => '+56 9'.$faker->unique()->randomNumber($nbDigits = 8,$strict = true),
        'telefono_casa' => '+45 2'.$faker->unique()->randomNumber($nbDigits = 6,$strict = true),
        'sistema_salud' => $faker->randomElement($array = array ('FONASA', 'Isapre')),
        'grupo_sanguineo' =>  $faker->randomElement($array = array ('A+','A-', 'B+','B-','AB+','AB-','O+','O-')),
        'cesfam_id' => 2,
        'user_id' => $id_user++,
    ];
  });
  $factory->state(Paciente::class, 'femenino_cesfam3', function (Faker $faker) {
    static $id_user=47;
    $fecha_nacimiento= $faker-> date($format = 'Y-m-d', $max = 'now');
    return [
        'nombres' => $faker->firstNameFemale .' '. $faker->firstNameFemale,
        'apellidos' => $faker->lastName .' '. $faker->lastName,
        'fecha_nacimiento' =>  $fecha_nacimiento,
        'edad' => Carbon::createFromDate($fecha_nacimiento)->age, 
        'sexo' => 'Femenino',
        'nacionalidad' => $faker->randomElement($array = array ('Chilena','Argentina','Paraguaya','Mexicana','Venezolana')),
        'lugar_nacimiento' => 'Temuco, Chile',
        'direccion' => $faker->streetAddress . ', ' .'Temuco, Chile',
        'telefono_celular' => '+56 9'.$faker->unique()->randomNumber($nbDigits = 8,$strict = true),
        'telefono_casa' => '+45 2'.$faker->unique()->randomNumber($nbDigits = 6,$strict = true),
        'sistema_salud' => $faker->randomElement($array = array ('FONASA', 'Isapre')),
        'grupo_sanguineo' =>  $faker->randomElement($array = array ('A+','A-', 'B+','B-','AB+','AB-','O+','O-')),
        'cesfam_id' => 3,
        'user_id' => $id_user++,
    ];
  });

$factory->state(Paciente::class, 'masculino_cesfam1', function (Faker $faker) {
    static $id_user=27;
    $fecha_nacimiento= $faker-> date($format = 'Y-m-d', $max = 'now');
    return [
        'nombres' => $faker->firstNameMale .' '. $faker->firstNameMale,
        'apellidos' => $faker->lastName .' '. $faker->lastName,
        'fecha_nacimiento' => $fecha_nacimiento,
        'edad' => Carbon::createFromDate($fecha_nacimiento)->age,
        'sexo' => 'Masculino',
        'nacionalidad' => $faker->randomElement($array = array ('Chilena','Argentina','Paraguaya','Mexicana','Venezolana')),
        'lugar_nacimiento' => 'Temuco, Chile',
        'direccion' => $faker->streetAddress . ', ' .'Temuco, Chile',
        'telefono_celular' => '+56 9'.$faker->unique()->randomNumber($nbDigits = 8,$strict = true),
        'telefono_casa' => '+45 2'.$faker->unique()->randomNumber($nbDigits = 6,$strict = true),
        'sistema_salud' => $faker->randomElement($array = array ('FONASA', 'Isapre')),
        'grupo_sanguineo' =>  $faker->randomElement($array = array ('A+','A-', 'B+','B-','AB+','AB-','O+','O-')),
        'cesfam_id' => 1,
        'user_id' => $id_user++,
    ];
  });
  $factory->state(Paciente::class, 'masculino_cesfam2', function (Faker $faker) {
    static $id_user=42;
    $fecha_nacimiento= $faker-> date($format = 'Y-m-d', $max = 'now');
    return [
        'nombres' => $faker->firstNameMale .' '. $faker->firstNameMale,
        'apellidos' => $faker->lastName .' '. $faker->lastName,
        'fecha_nacimiento' => $fecha_nacimiento,
        'edad' => Carbon::createFromDate($fecha_nacimiento)->age,
        'sexo' => 'Masculino',
        'nacionalidad' => $faker->randomElement($array = array ('Chilena','Argentina','Paraguaya','Mexicana','Venezolana')),
        'lugar_nacimiento' => 'Temuco, Chile',
        'direccion' => $faker->streetAddress . ', ' .'Temuco, Chile',
        'telefono_celular' => '+56 9'.$faker->unique()->randomNumber($nbDigits = 8,$strict = true),
        'telefono_casa' => '+45 2'.$faker->unique()->randomNumber($nbDigits = 6,$strict = true),
        'sistema_salud' => $faker->randomElement($array = array ('FONASA', 'Isapre')),
        'grupo_sanguineo' =>  $faker->randomElement($array = array ('A+','A-', 'B+','B-','AB+','AB-','O+','O-')),
        'cesfam_id' => 2,
        'user_id' => $id_user++,
    ];
  });
  $factory->state(Paciente::class, 'masculino_cesfam3', function (Faker $faker) {
    static $id_user=52;
    $fecha_nacimiento= $faker-> date($format = 'Y-m-d', $max = 'now');
    return [
        'nombres' => $faker->firstNameMale .' '. $faker->firstNameMale,
        'apellidos' => $faker->lastName .' '. $faker->lastName,
        'fecha_nacimiento' => $fecha_nacimiento,
        'edad' => Carbon::createFromDate($fecha_nacimiento)->age,
        'sexo' => 'Masculino',
        'nacionalidad' => $faker->randomElement($array = array ('Chilena','Argentina','Paraguaya','Mexicana','Venezolana')),
        'lugar_nacimiento' => 'Temuco, Chile',
        'direccion' => $faker->streetAddress . ', ' .'Temuco, Chile',
        'telefono_celular' => '+56 9'.$faker->unique()->randomNumber($nbDigits = 8,$strict = true),
        'telefono_casa' => '+45 2'.$faker->unique()->randomNumber($nbDigits = 6,$strict = true),
        'sistema_salud' => $faker->randomElement($array = array ('FONASA', 'Isapre')),
        'grupo_sanguineo' =>  $faker->randomElement($array = array ('A+','A-', 'B+','B-','AB+','AB-','O+','O-')),
        'cesfam_id' => 3,
        'user_id' => $id_user++,
    ];
  });