<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use \Freshwork\ChileanBundle\Rut;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
function generarRut($tipoUser){

    if($tipoUser==0){
        $random_number = rand(6000000, 20000000);
    }elseif($tipoUser==1){
        $random_number = rand(3000000, 25000000);
    }
    $rut = new Rut($random_number); 
    return  $rut->fix()->format();
}
$factory->define(User::class, function (Faker $faker) {   
    return [
        'tipo_user' =>'Funcionario',
        'rut' => generarRut(0),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt('sigati14'), // password
        'remember_token' => Str::random(10),
    ];
  });
  
$factory->state(User::class, 'funcionario', function (Faker $faker) {   
    return [
        'tipo_user' =>'Funcionario',
        'rut' => generarRut(0),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt('sigati14'), // password
        'remember_token' => Str::random(10),
    ];
  });

$factory->state(User::class,'paciente', function (Faker $faker) {   
    return [
        'tipo_user' =>'Paciente',
        'rut' => generarRut(1),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt('sigati14'), // password
        'remember_token' => Str::random(10),
    ];
  });

  