<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Funcionario;
use Faker\Generator as Faker;


$factory->define(Funcionario::class, function (Faker $faker) {   
    return [
        'nombres' => $faker->firstNameMale .' '. $faker->firstNameMale,
        'apellidos' => $faker->lastName .' '. $faker->lastName,
        'titulo' => Str::random(8),
        'sexo' => 1, 
        'firma_id' => 1,
        'user_id' => 1,
    ];
});
$factory->state(Funcionario::class, 'femenino', function (Faker $faker) {

    $profesion=['Médica General', 'Médica Pediatra','Kinesióloga','Enfermera','Nutricionista','Tecnóloga Médica','Médica Oftalmóloga','Tecnóloga Médica'];
    static $idProfesion = 0;
    static $orderFirma = 1; 
    static $orderUser = 1;
    return [
        'nombres' => $faker->firstNameFemale .' '. $faker->firstNameFemale,
        'apellidos' => $faker->lastName .' '. $faker->lastName,
        'titulo' => $profesion[$idProfesion++],
        'sexo' => 'Femenino', 
        'firma_id' => $orderFirma++,
        'user_id' => $orderUser++,
    ];
  });
$factory->state(Funcionario::class, 'masculino', function (Faker $faker) {
    $profesion=['Enfermero', 'Médico General','Médico General','Médico Pedriatra','Enfermero','Nutricionista','Enfermero','Nutricionista'];
    static $idProfesion = 0;
    static $orderFirma = 9; 
    static $orderUser = 9;
    return [
        'nombres' => $faker->firstNameMale .' '. $faker->firstNameMale,
        'apellidos' => $faker->lastName .' '. $faker->lastName,
        'titulo' => $profesion[$idProfesion++],
        'sexo' => 'Masculino', 
        'firma_id' => $orderFirma++,
        'user_id' => $orderUser++,
    ];
  });



  