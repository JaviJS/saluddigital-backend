<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//jwt
Route::group(['middleware' => ['jwt.auth'],'prefix' => 'v1'], function(){
    });

Route::group(['prefix' => 'v1'], function () {
    Route::post('/auth/refresh','TokensController@refreshToken');
    Route::get('/auth/logout','TokensController@logout');
    Route::post('/auth/login','TokensController@login');
});

Route::get('pacientes', 'PacienteController@index');
Route::get('paciente/{idUser}', 'PacienteController@show');
Route::get('buscarPaciente/{idPaciente}', 'PacienteController@buscarPaciente');
Route::get('paciente/enfermedades/{id}', 'PacienteController@buscarEnfermedadesPaciente');
Route::get('paciente/medicamentos/{id}', 'PacienteController@buscarMedicamentosPaciente');
Route::get('listarColumnas', 'PacienteController@listarColumnas');

Route::get('listarCesfam', 'CesfamController@index');
Route::get('verCesfam/{id}', 'CesfamController@show');
Route::get('verEspecialidadesCesfam/{id}', 'CesfamController@cesfamEspecialidades');
Route::get('verMedicoGeneralCesfam/{id}', 'CesfamController@especialidadMedicoGeneralCesfam');

Route::post('createPaciente', 'PacienteController@store');
Route::post('createFuncionario', 'FuncionarioController@store');

Route::get('citasPaciente/{id}', 'CitaMedicaController@citasMedicasPaciente');
Route::get('citasFinalizadasPaciente/{id}', 'CitaMedicaController@citasMedicasFinalizadasPaciente');
Route::get('citasPendientesPaciente/{id}', 'CitaMedicaController@citasMedicasPendientesPaciente');
Route::get('verCita/{id}', 'CitaMedicaController@show');
Route::put('agregarObservacion/{id}', 'CitaMedicaController@agregarObservaciónCita');
Route::post('agregarCitaMedica', 'CitaMedicaController@store');
Route::put('cancelarCita/{id}', 'CitaMedicaController@cancelarCita');
Route::put('reasignarCita/{id}', 'CitaMedicaController@reasignarCita');
Route::get('buscarFechasCitaMedica', 'CitaMedicaController@buscarFechaCitaMedica');
Route::get('buscarHorasCitaMedica', 'CitaMedicaController@buscarHorasDisponibles');
Route::get('buscarFechasCitaParaPacientes', 'CitaMedicaController@buscarFechasCitaParaPacientes');
Route::post('agregarDocumentosCita/{id}', 'CitaMedicaController@agregarDocumentosCita');
Route::get('verDocumentosCita/{id}', 'CitaMedicaController@verDocumentosCita');



Route::get('buscarFuncionario/{id}', 'FuncionarioController@buscarFuncionario');

Route::get('turnoFuncionario/{id}', 'TurnoController@turnosFuncionario');
Route::get('citasTurnoFuncionario/{id}', 'TurnoController@citasTurnoFuncionario');