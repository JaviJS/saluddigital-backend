<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Firma extends Model
{
    protected $table = 'firma';
	protected $primaryKey = 'id_firma';
	protected $fillable = ['ruta'];
    public $timestamps = true;
    
    public function funcionario()
    {
        return $this->hasOne('App\Model\Funcionario','firma_id');
    }
}
