<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FuncionarioCesfam extends Model
{
    protected $table = 'funcionario_cesfam';
	protected $primaryKey = 'id_funcionario_cesfam';
	protected $fillable = ['funcionario_id','cesfam_id'];
    public $timestamps = true;

    public function cesfam()
    {
        return $this->belongsTo('App\Model\Cesfam','cesfam_id');
    }

    public function funcionario()
    {
        return $this->belongsTo('App\Model\Funcionario','funcionario_id');
    }

    public function especialidadesFuncionarioCesfams()
    {
        return $this->hasMany('App\Model\EspecialidadFuncionarioCesfam','funcionario_cesfam_id');
    }
}
