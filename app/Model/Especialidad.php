<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Especialidad extends Model
{
    protected $table = 'especialidad';
	protected $primaryKey = 'id_especialidad';
	protected $fillable = ['nombre_especialidad'];
    public $timestamps = true;

   
    public function especialidadesCesfam()
    {
        return $this->hasMany('App\Model\EspecialidadCesfam','especialidad_id');
    }
}
