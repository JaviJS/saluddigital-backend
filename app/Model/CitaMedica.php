<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CitaMedica extends Model
{
    protected $table = 'cita_medica';
	protected $primaryKey = 'id_cita_medica';
	protected $fillable = ['nro_espera','fecha_inicio','fecha_final','observacion','estado','especialidad_cesfam_id','sala_id','paciente_id'];
    public $timestamps = true;

    public function paciente()
    {
        return $this->belongsTo('App\Model\Paciente','paciente_id');
    }
    public function sala()
    {
        return $this->belongsTo('App\Model\Sala','sala_id');
    }
    public function especialidadCesfam()
    {
        return $this->belongsTo('App\Model\EspecialidadCesfam','especialidad_cesfam_id');
    }
    public function documentos()
    {
        return $this->hasMany('App\Model\Documento','cita_medica_id');
    }


}
