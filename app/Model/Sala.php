<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sala extends Model
{
    protected $table = 'sala';
	protected $primaryKey = 'id_sala';
	protected $fillable = ['nombre_sala','nombre_pabellon','especialidad_cesfam_id'];
    public $timestamps = true;

    public function citasMedicas()
    {
        return $this->hasMany('App\Model\CitaMedica','sala_id');
    }
    public function especialidadCesfam()
    {
        return $this->belongsTo('App\Model\EspecialidadCesfam','especialidad_cesfam_id');
    }
    public function turnos()
    {
        return $this->hasMany('App\Model\Turno','sala_id');
    }
}
