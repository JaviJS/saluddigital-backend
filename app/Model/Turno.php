<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{
    protected $table = 'turno';
	protected $primaryKey = 'id_turno';
	protected $fillable = ['fecha_inicio','fecha_fin','especialidad_funcionario_cesfam_id','sala_id'];
    public $timestamps = true;

    public function sala()
    {
        return $this->belongsTo('App\Model\Sala','sala_id');
    }
    public function especialidadesFuncionariosCesfam()
    {
        return $this->hasMany('App\Model\EspecialidadFuncionarioCesfam','funcionario_cesfam_id');
    }
}
