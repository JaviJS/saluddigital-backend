<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EspecialidadCesfam extends Model
{
    protected $table = 'especialidad_cesfam';
	protected $primaryKey = 'id_especialidad_cesfam';
	protected $fillable = ['cupo','especialidad_id','cesfam_id'];
    public $timestamps = true;

    public function cesfam()
    {
        return $this->belongsTo('App\Model\Cesfam','cesfam_id');
    }
    public function especialidad()
    {
        return $this->belongsTo('App\Model\Especialidad','especialidad_id');
    }

    public function especialidadesFuncionariosCesfam()
    {
        return $this->hasMany('App\Model\EspecialidadFuncionarioCesfam','especialidad_cesfam_id');
    }
    public function sala()
    {
        return $this->hasOne('App\Model\Sala','especialidad_cesfam_id');
    }
    public function citasMedicas()
    {
        return $this->hasMany('App\Model\CitaMedica','especialidad_cesfam_id');
    }
}
