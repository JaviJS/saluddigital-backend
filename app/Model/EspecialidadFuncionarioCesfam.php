<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EspecialidadFuncionarioCesfam extends Model
{
    protected $table = 'especialidad_funcionario_cesfam';
	protected $primaryKey = 'id_especialidad_funcionario_cesfam';
	protected $fillable = ['funcionario_cesfam_id','especialidad_cesfam_id'];
    public $timestamps = true;


    public function especialidadCesfam()
    {
        return $this->belongsTo('App\Model\EspecialidadCesfam','especialidad_cesfam_id');
    }

    public function turno()
    {
        return $this->belongsTo('App\Model\Turno','funcionario_cesfam_id');
    }

    public function funcionarioCesfam()
    {
        return $this->belongsTo('App\Model\FuncionarioCesfam','funcionario_cesfam_id');
    }
}
