<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
   
    protected $table = 'paciente';
	protected $primaryKey = 'id_paciente';
	protected $fillable = ['nombres','apellidos','fecha_nacimiento','edad','sexo','nacionalidad','lugar_nacimiento','direccion','telefono_celular','telefono_casa','sistema_salud','grupo_sanguineo','cesfam_id','user_id'];
	public $timestamps = true;


	public function cesfam()
    {
        return $this->belongsTo('App\Model\Cesfam','cesfam_id');
    }
    public function citasMedicas()
    {
        return $this->hasMany('App\Model\CitaMedica','paciente_id');
    }
    public function user()
    {
        return $this->belongsTo('App\Model\User','user_id');
    }

    public function pacienteEnfermedades()
    {
        return $this->hasMany('App\Model\PacienteEnfermedad','paciente_id');
    }
}
