<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    protected $table = 'documento';
	protected $primaryKey = 'id_documento';
	protected $fillable = ['ruta','cita_medica_id'];
    public $timestamps = true;

    public function citaMedica()
    {
        return $this->belongsTo('App\Model\CitaMedica','cita_medica_id');
    }
}
