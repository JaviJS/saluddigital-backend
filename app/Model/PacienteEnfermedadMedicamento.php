<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PacienteEnfermedadMedicamento extends Model
{
    protected $table = 'paciente_enfermedad_medicamento';
	protected $primaryKey = 'id_paciente_enfermedad_medicamento';
	protected $fillable = ['indicacion','medicamento_id','paciente_enfermedad_id'];
    public $timestamps = true;

    public function pacienteEnfermedad()
    {
        return $this->belongsTo('App\Model\PacienteEnfermedad','paciente_enfermedad_id');
    }
    public function medicamento()
    {
        return $this->belongsTo('App\Model\Medicamento','medicamento_id');
    }
}
