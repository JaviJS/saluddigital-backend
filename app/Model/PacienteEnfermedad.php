<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PacienteEnfermedad extends Model
{
    protected $table = 'paciente_enfermedad';
	protected $primaryKey = 'id_paciente_enfermedad';
	protected $fillable = ['fecha_inicio','paciente_id','enfermedad_id'];
    public $timestamps = true;

    public function paciente()
    {
        return $this->belongsTo('App\Model\Paciente','paciente_id');
    }
    public function enfermedad()
    {
        return $this->belongsTo('App\Model\Enfermedad','enfermedad_id');
    }
    public function pacienteEnfermedadMedicamentos()
    {
        return $this->hasMany('App\Model\PacienteEnfermedadMedicamento','paciente_enfermedad_id');
    }
}
