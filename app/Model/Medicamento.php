<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Medicamento extends Model
{
    protected $table = 'medicamento';
	protected $primaryKey = 'id_medicamento';
	protected $fillable = ['nombre_medicamento'];
    public $timestamps = true;

    public function pacientesEnfermedadesMedicamentos()
    {
        return $this->hasMany('App\Model\PacienteEnfermedadMedicamento','medicamento_id');
    }
    public function enfermedades()
    {
        return $this->belongsToMany('App\Model\Enfermedad', 'enfermedad_id', 'medicamento_id');
    }
}

