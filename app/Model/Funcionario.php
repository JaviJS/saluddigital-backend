<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Funcionario extends Model
{
    protected $table = 'funcionario';
	protected $primaryKey = 'id_funcionario';
	protected $fillable = ['nombres','apellidos','titulo','sexo','firma_id','user_id'];
	public $timestamps = true;

	public function firma()
    {
        return $this->belongsTo('App\Model\Firma','firma_id');
    }
    public function user()
    {
        return $this->belongsTo('App\Model\User','user_id');
    }
    public function funcionarioCesfams()
    {
        return $this->hasMany('App\Model\FuncionarioCesfam','funcionario_id');
    }
}
