<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cesfam extends Model
{
    protected $table = 'cesfam';
	protected $primaryKey = 'id_cesfam';
	protected $fillable = ['nombre_cesfam','dirección','comuna','ciudad','region','telefono'];
    public $timestamps = true;

    public function pacientes()
    {
        return $this->hasMany('App\Model\Paciente','cesfam_id');
    }
    public function especialidadesCesfam()
    {
        return $this->hasMany('App\Model\EspecialidadCesfam','cesfam_id');
    }
    public function funcionariosCesfam()
    {
        return $this->hasMany('App\Model\FuncionarioCesfam','cesfam_id');
    }
}
