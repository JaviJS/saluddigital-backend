<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Enfermedad extends Model
{
    protected $table = 'enfermedad';
	protected $primaryKey = 'id_enfermedad';
	protected $fillable = ['nombre_enfermedad'];
    public $timestamps = true;

    public function enfermedadPacientes()
    {
        return $this->hasMany('App\Model\PacienteEnfermedad','enfermedad_id');
    }

    public function medicamentos()
    {
        return $this->belongsToMany('App\Model\Medicamento', 'enfermedad_id', 'medicamento_id');
    }
}
