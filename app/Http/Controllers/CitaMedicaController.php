<?php

namespace App\Http\Controllers;

use App\Services\CitaMedicaService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CitaMedicaController extends Controller
{
    private $citaMedicaService;

    public function __construct(CitaMedicaService $citaMedicaService)
    {
        $this->citaMedicaService = $citaMedicaService;
    }
    public function index()
    {
        $data = $this->citaMedicaService->all();
        return response()->json($data->data, $data->status);
    }
    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $data = $this->citaMedicaService->create($request);
        return response()->json($data->data, $data->status);
    }


    public function show($id)
    {
        $data = $this->citaMedicaService->find($id);
        return response()->json($data->data, $data->status);
    }

    public function edit($id)
    {
      //
    }

  
    public function update(Request $request, $id)
    {
        $data = $this->citaMedicaService->update($request, $id);
        return response()->json($data->data, $data->status);

    }

    public function destroy($id)
    {
        $data = $this->citaMedicaService->delete($id);
        return response()->json($data->data, $data->status);
    }

    public function citasMedicasPaciente($id)
    {
        $data = $this->citaMedicaService->citasMedicasPaciente($id);
        return response()->json($data->data, $data->status);
    }
    public function citasMedicasPendientesPaciente($id)
    {
        $data = $this->citaMedicaService->citasMedicasPendientesPaciente($id);
        return response()->json($data->data, $data->status);
    }
    public function citasMedicasFinalizadasPaciente($id)
    {
        $data = $this->citaMedicaService->citasMedicasFinalizadasPaciente($id);
        return response()->json($data->data, $data->status);
    }

    public function agregarObservaciónCita(Request $request, $id)
    {
        $data = $this->citaMedicaService->agregarObservaciónCita($request,$id);
        return response()->json($data->data, $data->status);
    }

    public function cancelarCita(Request $request, $id)
    {
        $data = $this->citaMedicaService->cancelarCita($request,$id);
        return response()->json($data->data, $data->status);
    }

    public function buscarFechaCitaMedica(Request $request)
    {
        $data = $this->citaMedicaService->buscarFechaCitaMedica($request);
        return response()->json($data->data, $data->status);
    }

    public function buscarHorasDisponibles(Request $request)
    {
        $data = $this->citaMedicaService->buscarHorasDisponibles($request);
        return response()->json($data->data, $data->status);
    }
    public function reasignarCita(Request $request,$id)
    {
        $data = $this->citaMedicaService->reasignarCita($request,$id);
        return response()->json($data->data, $data->status);
    }
    public function buscarFechasCitaParaPacientes(Request $request)
    {
        $data = $this->citaMedicaService->buscarFechasCitaParaPacientes($request);
        return response()->json($data->data, $data->status);
    }   
    public function agregarDocumentosCita(Request $request,$id)
    {
        $data = $this->citaMedicaService->agregarDocumentosCita($request,$id);
        return response()->json($data->data, $data->status);
    }  
    
    public function verDocumentosCita($id)
    {
        $data = $this->citaMedicaService->verDocumentosCita($id);
        return response()->json($data->data, $data->status);
    } 
}
