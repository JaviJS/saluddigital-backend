<?php

namespace App\Http\Controllers;

use App\Services\FuncionarioService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FuncionarioController extends Controller
{
    private $funcionarioService;

    public function __construct(FuncionarioService $funcionarioService)
    {
        $this->funcionarioService = $funcionarioService;
    }
    public function index()
    {
        $data = $this->funcionarioService->all();
        return response()->json($data->data, $data->status);
    }
    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $data = $this->funcionarioService->create($request);
        return response()->json($data->data, $data->status);
    }


    public function show($id)
    {
        $data = $this->funcionarioService->find($id);
        return response()->json($data->data, $data->status);
    }

    public function edit($id)
    {
      //
    }

  
    public function update(Request $request, $id)
    {
        $data = $this->funcionarioService->update($request, $id);
        return response()->json($data->data, $data->status);

    }

    public function destroy($id)
    {
        $data = $this->funcionarioService->delete($id);
        return response()->json($data->data, $data->status);
    }

    public function buscarFuncionario($id)
    {
        $data = $this->funcionarioService->buscarFuncionario($id);
        return response()->json($data->data, $data->status);
    }
}
