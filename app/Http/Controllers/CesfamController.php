<?php

namespace App\Http\Controllers;
use App\Services\CesfamService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class CesfamController extends Controller
{
    private $cesfamService;

    public function __construct(CesfamService $cesfamService)
    {
        $this->cesfamService = $cesfamService;
    }

    public function index()
    {
        $data = $this->cesfamService->all();
        return response()->json($data->data, $data->status);
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $data = $this->cesfamService->create($request);
        return response()->json($data->data, $data->status);
    }


    public function show($id)
    {
        $data = $this->cesfamService->find($id);
        return response()->json($data->data, $data->status);
    }

    public function edit($id)
    {
      //
    }

  
    public function update(Request $request, $id)
    {
        $data = $this->cesfamService->update($request, $id);
        return response()->json($data->data, $data->status);

    }

    public function destroy($id)
    {
        $data = $this->cesfamService->delete($id);
        return response()->json($data->data, $data->status);
    }

    public function cesfamEspecialidades($id)
    {
        $data = $this->cesfamService->cesfamEspecialidades($id);
        return response()->json($data->data, $data->status);
    }

    public function especialidadMedicoGeneralCesfam($id)
    {
        $data = $this->cesfamService->especialidadMedicoGeneralCesfam($id);
        return response()->json($data->data, $data->status);
    }
}
