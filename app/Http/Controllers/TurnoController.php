<?php

namespace App\Http\Controllers;


use App\Services\TurnoService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
class TurnoController extends Controller
{
    private $turnoService;

    public function __construct(TurnoService $turnoService)
    {
        $this->turnoService = $turnoService;
    }
    public function index()
    {
        $data = $this->turnoService->all();
        return response()->json($data->data, $data->status);
    }
    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $data = $this->turnoService->create($request);
        return response()->json($data->data, $data->status);
    }


    public function show($id)
    {
        $data = $this->turnoService->find($id);
        return response()->json($data->data, $data->status);
    }

    public function edit($id)
    {
      //
    }

  
    public function update(Request $request, $id)
    {
        $data = $this->turnoService->update($request, $id);
        return response()->json($data->data, $data->status);

    }

    public function destroy($id)
    {
        $data = $this->turnoService->delete($id);
        return response()->json($data->data, $data->status);
    }

    public function turnosFuncionario($id)
    {
        $data = $this->turnoService->turnosFuncionario($id);
        return response()->json($data->data, $data->status);
    }

    public function citasTurnoFuncionario($id)
    {
        $data = $this->turnoService->citasTurnoFuncionario($id);
        return response()->json($data->data, $data->status);
    }
}
