<?php

namespace App\Http\Controllers;
//use App\Http\Requests\Paciente\PacienteRequest;
use App\Services\PacienteService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PacienteController extends Controller
{


    private $pacienteService;

    public function __construct(PacienteService $pacienteService)
    {
        $this->pacienteService = $pacienteService;
    }

    public function index()
    {
        $data = $this->pacienteService->all();
        return response()->json($data->data, $data->status);
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $data = $this->pacienteService->create($request);
        return response()->json($data->data, $data->status);
    }


    public function show($id)
    {
        $data = $this->pacienteService->find($id);
        return response()->json($data->data, $data->status);
    }
    public function buscarPaciente($id)
    {
        $data = $this->pacienteService->buscarPaciente($id);
        return response()->json($data->data, $data->status);
    }
    public function edit($id)
    {
      //
    }

  
    public function update(Request $request, $id)
    {
        $data = $this->pacienteService->update($request, $id);
        return response()->json($data->data, $data->status);

    }

    public function destroy($id)
    {
        $data = $this->pacienteService->delete($id);
        return response()->json($data->data, $data->status);
    }

    public function buscarEnfermedadesPaciente($id)
    {
        $data = $this->pacienteService->buscarEnfermedadesPaciente($id);
        return response()->json($data->data, $data->status);
    }
    public function buscarMedicamentosPaciente($id)
    {
        $data = $this->pacienteService->buscarMedicamentosPaciente($id);
        return response()->json($data->data, $data->status);
    }

    public function listarColumnas()
    {
        $data = $this->pacienteService->listarColumnas();
        return response()->json($data->data, $data->status);
    }
}