<?php

namespace App\Repositories\Cesfam;

use Illuminate\Support\Facades\DB;
use App\Model\Cesfam;
class CesfamRepository implements CesfamRepositoryInterface
{

    public function all()
    {      
        return Cesfam::all();
    }

    public function create(array $data)
    {
        return Cesfam::create($data);
    }
    public function update(array $data, $id)
    {
        return DB::table('cesfam')
            ->where('id_cesfam', $id)
            ->update(
                $data
            );
    }

    public function delete($id)
    {
        return DB::table('cesfam')
            ->where('id_cesfam', $id)
            ->delete();
    }

    public function find($id)
    {
        return Cesfam::all()->find($id);
    }

    public function cesfamEspecialidades($id)
    {

        return Cesfam::join('especialidad_cesfam', 'especialidad_cesfam.cesfam_id', '=', 'cesfam.id_cesfam')
        ->join('sala', 'sala.especialidad_cesfam_id', '=', 'especialidad_cesfam.id_especialidad_cesfam')
        ->join('especialidad', 'especialidad.id_especialidad', '=', 'especialidad_cesfam.especialidad_id')
        ->where('cesfam.id_cesfam','=', $id)
        ->select('sala.id_sala','sala.nombre_sala','especialidad_cesfam.cupo','especialidad_cesfam.id_especialidad_cesfam','especialidad.id_especialidad','especialidad.nombre_especialidad')->get();
    }

    public function especialidadMedicoGeneralCesfam($id)
    {
        return Cesfam::join('especialidad_cesfam', 'especialidad_cesfam.cesfam_id', '=', 'cesfam.id_cesfam')
        ->join('sala', 'sala.especialidad_cesfam_id', '=', 'especialidad_cesfam.id_especialidad_cesfam')
        ->join('especialidad', 'especialidad.id_especialidad', '=', 'especialidad_cesfam.especialidad_id')
        ->where('cesfam.id_cesfam','=', $id)
        ->where('especialidad.id_especialidad','=', 1)
        ->select('sala.id_sala','sala.nombre_sala','especialidad_cesfam.cupo','especialidad_cesfam.id_especialidad_cesfam','especialidad.id_especialidad','especialidad.nombre_especialidad')->get();
    }
    
}