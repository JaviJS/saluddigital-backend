<?php

namespace App\Repositories\Cesfam;

use App\Repositories\RepositoryInterface;

interface CesfamRepositoryInterface extends RepositoryInterface
{
    public function cesfamEspecialidades($id);
    public function especialidadMedicoGeneralCesfam($id);    
}