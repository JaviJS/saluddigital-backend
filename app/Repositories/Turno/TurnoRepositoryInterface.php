<?php

namespace App\Repositories\Turno;

use App\Repositories\RepositoryInterface;

interface TurnoRepositoryInterface extends RepositoryInterface
{
   public function turnosFuncionario($id);
   public function citasTurnoFuncionario($data);
}