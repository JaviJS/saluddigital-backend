<?php

namespace App\Repositories\Turno;

use Illuminate\Support\Facades\DB;
use App\Model\CitaMedica;
use App\Model\Turno;
use App\Model\Funcionario;
use App\Model\EspecialidadFuncionarioCesfam;
class TurnoRepository implements TurnoRepositoryInterface
{

    public function all()
    {      
        return Turno::all();
    }

    public function create(array $data)
    {
        return Turno::create($data);
    }


    public function update(array $data, $id)
    {
        return DB::table('turno')
            ->where('id_turno', $id)
            ->update(
                $data
            );
    }

    public function delete($id)
    {
        return DB::table('turno')
            ->where('id_turno', $id)
            ->delete();
    }

    public function find($id)
    {
        return DB::table('turno')
            ->where('id_turno', $id)
            ->first();
    }

    public function turnosFuncionario($id){
        //'turno.id_turno','turno.fecha_inicio','turno.fecha_fin','sala.nombre_sala','sala.nombre_pabellon','especialidad.nombre_especialidad','cesfam.nombre_cesfam','cesfam.dirección','cesfam.comuna','cesfam.region'
        return Funcionario::where('funcionario.id_funcionario', $id)
        ->join('funcionario_cesfam', 'funcionario.id_funcionario', '=', 'funcionario_cesfam.funcionario_id')
        ->join('especialidad_funcionario_cesfam', 'funcionario_cesfam.id_funcionario_cesfam', '=', 'especialidad_funcionario_cesfam.funcionario_cesfam_id')
        ->join('especialidad_cesfam', 'especialidad_funcionario_cesfam.especialidad_cesfam_id', '=', 'especialidad_cesfam.id_especialidad_cesfam')
        ->join('turno', 'especialidad_funcionario_cesfam.id_especialidad_funcionario_cesfam', '=', 'turno.especialidad_funcionario_cesfam_id')
        ->join('especialidad', 'especialidad_cesfam.especialidad_id', '=', 'especialidad.id_especialidad')
        ->join('cesfam', 'especialidad_cesfam.cesfam_id', '=', 'cesfam.id_cesfam')
        ->join('sala', 'turno.sala_id', '=', 'sala.id_sala')
        ->select('especialidad_funcionario_cesfam.id_especialidad_funcionario_cesfam','turno.fecha_inicio','turno.fecha_fin','cesfam.id_cesfam','especialidad.id_especialidad','especialidad.nombre_especialidad')->get();
    }

    public function citasTurnoFuncionario($data){            
              
        return CitaMedica::join('paciente', 'cita_medica.paciente_id', '=', 'paciente.id_paciente')
           ->join('cesfam', 'paciente.cesfam_id', '=', 'cesfam.id_cesfam')
           ->join('especialidad_cesfam','especialidad_cesfam.id_especialidad_cesfam','=','cita_medica.especialidad_cesfam_id')
           ->join('especialidad','especialidad.id_especialidad','=','especialidad_cesfam.especialidad_id')
           ->join('sala','sala.id_sala','=','cita_medica.sala_id')
           ->where('cesfam.id_cesfam','=', $data['id_cesfam'])
           ->where('especialidad.id_especialidad','=', $data['id_especialidad'])
           ->where('cita_medica.estado', '!=' ,'Cancelada')
           ->whereBetween('fecha_inicio',  [$data['fecha_inicio'], $data['fecha_fin']])
           ->select('cita_medica.*','cesfam.*','paciente.nombres','paciente.apellidos','especialidad.nombre_especialidad','sala.nombre_sala')->get()->toArray();
    }
}