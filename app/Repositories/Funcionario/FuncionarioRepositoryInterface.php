<?php

namespace App\Repositories\Funcionario;

use App\Repositories\RepositoryInterface;

interface FuncionarioRepositoryInterface extends RepositoryInterface
{
   public function buscarFuncionario($id);
}