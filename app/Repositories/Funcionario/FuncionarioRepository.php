<?php

namespace App\Repositories\Funcionario;

use Illuminate\Support\Facades\DB;
use App\Model\Funcionario;
use App\Model\User;
use App\Model\Firma;
class FuncionarioRepository implements FuncionarioRepositoryInterface
{

    public function all()
    {      
        return Funcionario::all();
    }

    public function create(array $data)
    {
        
        $user = User::create($data['usuario']);
        $firma =Firma::create($data['firma']);
        $funcionario = new Funcionario($data['funcionario']);        
        $funcionario->user()->associate($user);
        $funcionario->firma()->associate($firma);
        $funcionario->save();
        return $user;
    }

    public function update(array $data, $id)
    {
        return DB::table('funcionario')
            ->where('id_funcionario', $id)
            ->update(
                $data
            );
    }

    public function delete($id)
    {
        return DB::table('funcionario')
            ->where('id_funcionario', $id)
            ->delete();
    }

    public function find($id)
    {
        return DB::table('funcionario')
            ->where('id_funcionario', $id)
            ->first();
    }

    public function buscarFuncionario($id){
        return Funcionario::where('user_id',$id)->with('firma')->get();
    }
  
}