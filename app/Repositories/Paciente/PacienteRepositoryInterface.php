<?php

namespace App\Repositories\Paciente;

use App\Repositories\RepositoryInterface;

interface PacienteRepositoryInterface extends RepositoryInterface
{
   public function buscarEnfermedadesPaciente($id);
   public function buscarMedicamentosPaciente($id);
   public function listarColumnas($columna);
   public function buscarPaciente($id);
}