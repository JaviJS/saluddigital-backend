<?php

namespace App\Repositories\Paciente;

use Illuminate\Support\Facades\DB;
use App\Model\Paciente;
use App\Model\User;
class PacienteRepository implements PacienteRepositoryInterface
{

    public function all()
    {      
        return Paciente::all();
    }

    public function create(array $data)
    {
        
        $user = User::create($data['usuario']);
        $paciente = new Paciente($data['paciente']);
        $paciente->user()->associate($user);
        $paciente->save();
        return $paciente;
    }
    public function update(array $data, $id)
    {
        return DB::table('paciente')
            ->where('id_paciente', $id)
            ->update(
                $data
            );
    }

    public function delete($id)
    {
        return DB::table('paciente')
            ->where('id_paciente', $id)
            ->delete();
    }

    public function find($id)
    {
        return Paciente::with('user')
        ->where('user_id',$id)->first();      
    }
   
    public function buscarEnfermedadesPaciente($id){
   
        return Paciente::join('paciente_enfermedad', 'paciente_enfermedad.paciente_id', '=', 'paciente.id_paciente')
        ->join('enfermedad', 'paciente_enfermedad.enfermedad_id', '=', 'enfermedad.id_enfermedad')
        ->where('paciente.id_paciente','=', $id)
        ->select('enfermedad.id_enfermedad','enfermedad.nombre_enfermedad','paciente_enfermedad.fecha_inicio')->get();
    }
    public function buscarMedicamentosPaciente($id){
        return Paciente::join('paciente_enfermedad', 'paciente_enfermedad.paciente_id', '=', 'paciente.id_paciente')
        ->join('enfermedad', 'paciente_enfermedad.enfermedad_id', '=', 'enfermedad.id_enfermedad')
        ->join('paciente_enfermedad_medicamento', 'paciente_enfermedad_medicamento.paciente_enfermedad_id', '=', 'paciente_enfermedad.id_paciente_enfermedad')
        ->join('medicamento', 'medicamento.id_medicamento', '=', 'paciente_enfermedad_medicamento.medicamento_id')
        ->where('paciente.id_paciente','=', $id)
        ->select('enfermedad.id_enfermedad','enfermedad.nombre_enfermedad','medicamento.id_medicamento','medicamento.nombre_medicamento','paciente_enfermedad_medicamento.indicacion')->get();
    }

    
    public function listarColumnas($columna){
        $arr = DB::select(DB::raw('SHOW COLUMNS FROM paciente Where Field = "'.$columna.'"'))[0]->Type;
        preg_match("/^enum\(\'(.*)\'\)$/", $arr, $matches);
        $enum = explode("','", $matches[1]);
        return $enum;
    }

    public function buscarPaciente($id){
        return Paciente::with('user')
        ->where('id_paciente',$id)->first();  
    }
}