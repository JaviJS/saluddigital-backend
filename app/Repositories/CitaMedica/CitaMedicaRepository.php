<?php

namespace App\Repositories\CitaMedica;

use Illuminate\Support\Facades\DB;
use App\Model\Paciente;
use App\Model\CitaMedica;
use App\Model\EspecialidadCesfam;
use App\Model\Documento;
class CitaMedicaRepository implements CitaMedicaRepositoryInterface
{

    public function all()
    {      
        return CitaMedica::all();
    }
 
    public function create(array $data)
    {
        return CitaMedica::create($data);
    }

    public function update(array $data, $id)
    {
        return DB::table('cita_medica')
            ->where('id_cita_medica', $id)
            ->update(
                $data
            );
    }

    public function delete($id)
    {
        return DB::table('cita_medica')
            ->where('id_cita_medica', $id)
            ->delete();
    }

    public function find($id)
    {
        return CitaMedica::join('sala','sala.id_sala','cita_medica.sala_id')
        ->join('especialidad_cesfam','especialidad_cesfam.id_especialidad_cesfam','cita_medica.especialidad_cesfam_id')
        ->join('especialidad','especialidad.id_especialidad','especialidad_cesfam.especialidad_id')
        ->where('id_cita_medica', '=', $id)
        ->get();

    }

    public function citasMedicasPaciente($id){
        return CitaMedica::with('sala')
        ->join('especialidad_cesfam','especialidad_cesfam.id_especialidad_cesfam','=','cita_medica.especialidad_cesfam_id')
        ->join('especialidad','especialidad.id_especialidad','=','especialidad_cesfam.especialidad_id')
        ->where('paciente_id', '=', $id)
        ->where('estado', '!=' ,'Cancelada')
        ->get();
    }
    public function citasMedicasPendientesPaciente($id){
        return CitaMedica::with('sala')
        ->join('especialidad_cesfam','especialidad_cesfam.id_especialidad_cesfam','=','cita_medica.especialidad_cesfam_id')
        ->join('especialidad','especialidad.id_especialidad','=','especialidad_cesfam.especialidad_id')
        ->where('paciente_id', '=', $id)
        ->where('estado', '=' ,'Pendiente')
        ->get();
    }
    public function citasMedicasFinalizadasPaciente($id){
        return CitaMedica::with('sala')
        ->join('especialidad_cesfam','especialidad_cesfam.id_especialidad_cesfam','=','cita_medica.especialidad_cesfam_id')
        ->join('especialidad','especialidad.id_especialidad','=','especialidad_cesfam.especialidad_id')
        ->where('paciente_id', '=', $id)
        ->where('estado', '=' ,'Finalizada')
        ->get();
    }    
    public function buscarFechaCitaMedica(array $data){
        return EspecialidadCesfam::join('sala','sala.especialidad_cesfam_id','=','especialidad_cesfam.id_especialidad_cesfam')
        ->join('cita_medica','cita_medica.sala_id','=','sala.id_sala')
        ->where('cita_medica.nro_espera','=',$data['cupo'])
        ->where('especialidad_cesfam.id_especialidad_cesfam', '=' ,$data['id_especialidad_cesfam'])
        ->where('cita_medica.fecha_inicio','>=', $data['fecha'])    
        ->select(DB::raw("DATE_FORMAT(cita_medica.fecha_inicio, '%Y-%m-%d') as fecha"))
        ->get()->toArray();
    }

    public function buscarFechasSinCupo(array $data){
        return EspecialidadCesfam::join('sala','sala.especialidad_cesfam_id','=','especialidad_cesfam.id_especialidad_cesfam')
        ->join('cita_medica','cita_medica.sala_id','=','sala.id_sala')
        ->where('especialidad_cesfam.id_especialidad_cesfam', '=' ,$data['id_especialidad_cesfam'])
        ->whereBetween('cita_medica.fecha_inicio',  [$data['fecha'].' 00:00:00', $data['fecha'].' 23:59:59'])     
        ->select(DB::raw("DATE_FORMAT(cita_medica.fecha_inicio, '%Y-%m-%d') as fecha"),'nro_espera')
        ->get()->toArray();
    }

    public function buscarCitasCanceladas(array $data){
        return EspecialidadCesfam::join('sala','sala.especialidad_cesfam_id','=','especialidad_cesfam.id_especialidad_cesfam')
        ->join('cita_medica','cita_medica.sala_id','=','sala.id_sala')
        ->where('especialidad_cesfam.id_especialidad_cesfam', '=' ,$data['id_especialidad_cesfam'])
        ->where('cita_medica.estado', '=', 'Cancelada')
        ->where('cita_medica.fecha_inicio','>=',$data['fecha'])     
        ->select(DB::raw("DATE_FORMAT(cita_medica.fecha_inicio, '%Y-%m-%d') as fecha"),'nro_espera')
        ->get()->toArray();
    }

    //buscar horas disponibles de citas medicas
    public function buscarHorasDisponibles(array $data){
        return CitaMedica::join('especialidad_cesfam','cita_medica.especialidad_cesfam_id','=','especialidad_cesfam.id_especialidad_cesfam')
        ->where('especialidad_cesfam.id_especialidad_cesfam', '=' ,$data['id_especialidad_cesfam'])
        ->whereBetween('cita_medica.fecha_inicio',  [$data['fecha'][0], $data['fecha'][1]])
        ->select(DB::raw("DATE_FORMAT(cita_medica.fecha_inicio, '%H:%i:00') as fecha"),'fecha_inicio')
        ->get()->toArray();
    }

    public function buscarCupoEspecialidad($id){
        return EspecialidadCesfam::where('id_especialidad_cesfam', '=' ,$id)
        ->select('cupo')
        ->get()->toArray();
    }

    //buscar citas de paciente // buscar disponibilidad de paciente
    public function disponibilidadCitaPaciente($id,$fecha){
        return CitaMedica::where('paciente_id', '=' ,$id)
        ->where('fecha_inicio', '=' ,$fecha)
        ->exists();
    }

    public function agregarDocumentosCitas(array $data){
        return Documento::create($data);
    }

    public function verDocumentosCita($id){
        return Documento::where('cita_medica_id', '=' ,$id)->get();
    }
}

