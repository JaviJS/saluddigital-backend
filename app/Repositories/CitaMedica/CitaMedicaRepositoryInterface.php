<?php

namespace App\Repositories\CitaMedica;

use App\Repositories\RepositoryInterface;

interface CitaMedicaRepositoryInterface extends RepositoryInterface
{
   public function citasMedicasPaciente($id);
   public function citasMedicasPendientesPaciente($id);
   public function citasMedicasFinalizadasPaciente($id);
   public function buscarFechaCitaMedica(array $data);
   public function buscarFechasSinCupo(array $data);
   public function buscarCitasCanceladas(array $data);
   public function buscarHorasDisponibles(array $data);
   public function buscarCupoEspecialidad($id);
   public function disponibilidadCitaPaciente($id,$fecha);
   public function agregarDocumentosCitas(array $data);     
   public function verDocumentosCita($id); 
}