<?php

namespace App\Helpers;

use DB;

class VerifyExistence
{
    /**
     * @param $id
     * @return bool
     */
    public static function response ($table_name, $column_name, $id): bool
    {
        $data = DB::table($table_name)->where($column_name, $id)->first();
        if (!isset($data)) return false;

        return true;
    }
}