<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\Paciente\PacienteRepositoryInterface',
            'App\Repositories\Paciente\PacienteRepository'
        );
        $this->app->bind(
            'App\Repositories\Funcionario\FuncionarioRepositoryInterface',
            'App\Repositories\Funcionario\FuncionarioRepository'
        );
        $this->app->bind(
            'App\Repositories\CitaMedica\CitaMedicaRepositoryInterface',
            'App\Repositories\CitaMedica\CitaMedicaRepository'
        );
        $this->app->bind(
            'App\Repositories\Turno\TurnoRepositoryInterface',
            'App\Repositories\Turno\TurnoRepository'
        );
        $this->app->bind(
            'App\Repositories\Cesfam\CesfamRepositoryInterface',
            'App\Repositories\Cesfam\CesfamRepository'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
