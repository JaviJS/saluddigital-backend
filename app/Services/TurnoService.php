<?php

namespace App\Services;

use App\Helpers\HttpResponse;
use App\Helpers\VerifyExistence;
use App\Repositories\Turno\TurnoRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
class TurnoService
{

    private $turnoRepository;

    public function __construct(TurnoRepositoryInterface $turnoRepository)
    {
        $this->turnoRepository = $turnoRepository;
    }


    public function all()
    {
        $turnos = $this->turnoRepository->all();
        return HttpResponse::response($turnos, 200);
    }

    public function create (Request $request)
    {

        try {

           
            $turno = $request->all();
            $turno['created_at'] = Carbon::now('America/Santiago');
            $status = $this->turnoRepository->create($turno);

            if (!$status)
            {
                return HttpResponse::response('Error al crear el turno', 500);
            }

            $turno = $this->turnoRepository->all();

            return HttpResponse::response($turno, 201);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }

    }


    public function update (Request $request, $turno_id)
    {
        
        try {

            if (!VerifyExistence::response('turno', 'id_turno', $turno_id))
            {
                return HttpResponse::response('Turno no encontrado', 404);
            }

            $turno  = $request->all();
            $turno['updated_at'] = Carbon::now('America/Santiago');

            $status = $this->turnoRepository->update($turno, $turno_id);

            if (!$status)
            {
                return HttpResponse::response('Error al actualizar el turno', 500);
            }

            $turnos = $this->turnoRepository->all();

            return HttpResponse::response($turnos, 200);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }


    }

    public function delete ($turno_id)
    {

        if (!VerifyExistence::response('turno', 'id_turno', $turno_id))
        {
            return HttpResponse::response('Turno no encontrada', 404);
        }

        $status = $this->turnoRepository->delete($turno_id);

        if (!$status)
        {
            return HttpResponse::response('Error al eliminar el turno', 500);
        }

        $turnos = $this->turnoRepository->all();

        return HttpResponse::response($turnos, 200);        
    }


    public function find ($turno_id)
    {
        if (!VerifyExistence::response('turno', 'id_turno', $turno_id))
        {
            return HttpResponse::response('Turno no encontrado', 404);
        }

        $turno =  $this->turnoRepository->find($turno_id);
        return HttpResponse::response($turno, 200);
    }


    public function turnosFuncionario($funcionario_id)
    {
        if (!VerifyExistence::response('funcionario', 'id_funcionario', $funcionario_id))
        {
            return HttpResponse::response('Funcionario no encontrado', 404);
        }

        $turnos = $this->turnoRepository->turnosFuncionario($funcionario_id);
        return HttpResponse::response($turnos, 200);
    }


    public function citasTurnoFuncionario($funcionario_id)
    {
        if (!VerifyExistence::response('funcionario', 'id_funcionario', $funcionario_id))
        {
            return HttpResponse::response('Funcionario no encontrado', 404);
        }

        $turnos = $this->turnoRepository->turnosFuncionario($funcionario_id);    

        $citasMedicasFuncionario = array();
        for ($i=0; $i <count($turnos) ; $i++) { 
            $citasTurnos = $this->turnoRepository->citasTurnoFuncionario($turnos[$i]);
            
           if(count($citasTurnos)!=0)
            {
                $citasMedicasFuncionario=array_merge($citasMedicasFuncionario, $citasTurnos);
            }                

        }
        return HttpResponse::response($citasMedicasFuncionario, 200);
    }

}