<?php

namespace App\Services;

use App\Helpers\HttpResponse;
use App\Helpers\VerifyExistence;
use App\Repositories\Cesfam\CesfamRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
class CesfamService
{

    private $cesfamRepository;

    public function __construct(CesfamRepositoryInterface $cesfamRepository)
    {
        $this->cesfamRepository = $cesfamRepository;
    }


    public function all()
    {
        $cesfams = $this->cesfamRepository->all();
        return HttpResponse::response($cesfams, 200);
    }

    public function create (Request $request)
    {

        try {

           
            $cesfam = $request->all();
            $cesfam['created_at'] = Carbon::now('America/Santiago');
            $status = $this->cesfamRepository->create($cesfam);

            if (!$status)
            {
                return HttpResponse::response('Error al crear el cesfam', 500);
            }

            $cesfams = $this->cesfamRepository->all();

            return HttpResponse::response($cesfams, 201);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }

    }


    public function update (Request $request, $cesfam_id)
    {
        
        try {

            if (!VerifyExistence::response('cesfam', 'id_cesfam', $cesfam_id))
            {
                return HttpResponse::response('Cesfam no encontrado', 404);
            }

            $cesfam  = $request->all();
            $cesfam['updated_at'] = Carbon::now('America/Santiago');

            $status = $this->cesfamRepository->update($cesfam, $cesfam_id);

            if (!$status)
            {
                return HttpResponse::response('Error al actualizar el cesfam', 500);
            }

            $cesfams = $this->cesfamRepository->all();

            return HttpResponse::response($cesfams, 200);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }


    }

    public function delete ($cesfam_id)
    {

        if (!VerifyExistence::response('cesfam', 'id_cesfam', $cesfam_id))
        {
            return HttpResponse::response('Cesfam no encontrado', 404);
        }

        $status = $this->cesfamRepository->delete($cesfam_id);

        if (!$status)
        {
            return HttpResponse::response('Error al eliminar el cesfam', 500);
        }

        $cesfams = $this->cesfamRepository->all();

        return HttpResponse::response($cesfams, 200);        
    }


    public function find ($cesfam_id)
    {
        if (!VerifyExistence::response('cesfam', 'id_cesfam', $cesfam_id))
        {
            return HttpResponse::response('Cesfam no encontrado', 404);
        }

        $cesfam =  $this->cesfamRepository->find($cesfam_id);
        return HttpResponse::response($cesfam, 200);
    }

    public function cesfamEspecialidades ($cesfam_id)
    {
        if (!VerifyExistence::response('cesfam', 'id_cesfam', $cesfam_id))
        {
            return HttpResponse::response('Cesfam no encontrado', 404);
        }

        $cesfam =  $this->cesfamRepository->cesfamEspecialidades($cesfam_id);
        return HttpResponse::response($cesfam, 200);
    }

    public function especialidadMedicoGeneralCesfam ($cesfam_id)
    {
        if (!VerifyExistence::response('cesfam', 'id_cesfam', $cesfam_id))
        {
            return HttpResponse::response('Cesfam no encontrado', 404);
        }

        $cesfam =  $this->cesfamRepository->especialidadMedicoGeneralCesfam($cesfam_id);
        return HttpResponse::response($cesfam, 200);
    }
}