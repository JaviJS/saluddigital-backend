<?php

namespace App\Services;

use App\Helpers\HttpResponse;
use App\Helpers\VerifyExistence;
use App\Repositories\Funcionario\FuncionarioRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Crypt;
class FuncionarioService
{

    private $funcionarioRepository;

    public function __construct(FuncionarioRepositoryInterface $funcionarioRepository)
    {
        $this->funcionarioRepository = $funcionarioRepository;
    }


    public function all()
    {
        $funcionarios = $this->funcionarioRepository->all();
        return HttpResponse::response($funcionarios, 200);
    }

    public function create (Request $request)
    {

        try {

           
            $funcionario['usuario'] = $request['usuario'];
            $funcionario['usuario']['password']= Crypt::encryptString($request->password);
            $funcionario['usuario']['created_at'] = Carbon::now('America/Santiago');
            
            $funcionario['funcionario'] = $request['funcionario'];
            $funcionario['funcionario']['created_at'] = Carbon::now('America/Santiago');

            $funcionario['firma'] = $request['firma'];
            $funcionario['firma']['created_at'] = Carbon::now('America/Santiago');

            $status = $this->funcionarioRepository->create($funcionario);

            if (!$status)
            {
                return HttpResponse::response('Error al crear el funcionario', 500);
            }

            $funcionarios = $this->funcionarioRepository->all();

            return HttpResponse::response($funcionarios, 201);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }

    }


    public function update (Request $request, $funcionario_id)
    {
        
        try {

            if (!VerifyExistence::response('funcionario', 'id_funcionario', $funcionario_id))
            {
                return HttpResponse::response('Funcionario no encontrado', 404);
            }

            $funcionario  = $request->all();
            $funcionario['updated_at'] = Carbon::now('America/Santiago');

            $status = $this->funcionarioRepository->update($funcionario, $funcionario_id);

            if (!$status)
            {
                return HttpResponse::response('Error al actualizar el funcionario', 500);
            }

            $funcionarios = $this->funcionarioRepository->all();

            return HttpResponse::response($funcionarios, 200);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }


    }

    public function delete ($funcionario_id)
    {

        if (!VerifyExistence::response('funcionario', 'id_funcionario', $funcionario_id))
        {
            return HttpResponse::response('Funcionario no encontrado', 404);
        }

        $status = $this->funcionarioRepository->delete($funcionario_id);

        if (!$status)
        {
            return HttpResponse::response('Error al eliminar el funcionario', 500);
        }

        $funcionarios = $this->funcionarioRepository->all();

        return HttpResponse::response($funcionarios, 200);        
    }


    public function find ($funcionario_id)
    {
        if (!VerifyExistence::response('funcionario', 'id_funcionario', $funcionario_id))
        {
            return HttpResponse::response('Funcionario no encontrado', 404);
        }

        $funcionario =  $this->funcionarioRepository->find($funcionario_id);
        return HttpResponse::response($funcionario, 200);
    }

    public function buscarFuncionario($id_user){
        if (!VerifyExistence::response('users', 'id', $id_user))
        {
            return HttpResponse::response('Usuario no no encontrado', 404);
        }

        $funcionario =  $this->funcionarioRepository->buscarFuncionario($id_user);
        return HttpResponse::response($funcionario, 200);
    }

}