<?php

namespace App\Services;

use App\Helpers\HttpResponse;
use App\Helpers\VerifyExistence;
use App\Repositories\Paciente\PacienteRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Crypt;
class PacienteService
{

    private $pacienteRepository;

    public function __construct(PacienteRepositoryInterface $pacienteRepository)
    {
        $this->pacienteRepository = $pacienteRepository;
    }


    public function all()
    {
        $pacientes = $this->pacienteRepository->all();
        return HttpResponse::response($pacientes, 200);
    }

    public function create (Request $request)
    {

        try {

           
            $paciente['usuario'] = $request['usuario'];
            $paciente['usuario']['password']= bcrypt($request['usuario']['password']);
            $paciente['usuario']['created_at'] = Carbon::now('America/Santiago');
            
            $paciente['paciente'] = $request['paciente'];
            $paciente['paciente']['created_at'] = Carbon::now('America/Santiago');

            $status = $this->pacienteRepository->create($paciente);

            if (!$status)
            {
                return HttpResponse::response('Error al crear el paciente', 500);
            }

            $pacientes = $this->pacienteRepository->all();

            return HttpResponse::response($pacientes, 201);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }

    }


    public function update (Request $request, $paciente_id)
    {
        
        try {

            if (!VerifyExistence::response('paciente', 'id_paciente', $paciente_id))
            {
                return HttpResponse::response('Paciente no encontrado', 404);
            }

            $paciente  = $request->all();
            $paciente['updated_at'] = Carbon::now('America/Santiago');

            $status = $this->pacienteRepository->update($paciente, $paciente_id);

            if (!$status)
            {
                return HttpResponse::response('Error al actualizar el paciente', 500);
            }

            $pacientes = $this->pacienteRepository->all();

            return HttpResponse::response($pacientes, 200);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }


    }

    public function delete ($paciente_id)
    {

        if (!VerifyExistence::response('paciente', 'id_paciente', $paciente_id))
        {
            return HttpResponse::response('Paciente no encontrado', 404);
        }

        $status = $this->pacienteRepository->delete($paciente_id);

        if (!$status)
        {
            return HttpResponse::response('Error al eliminar el paciente', 500);
        }

        $pacientes = $this->pacienteRepository->all();

        return HttpResponse::response($pacientes, 200);        
    }


    public function find ($id_user)
    {
        if (!VerifyExistence::response('users', 'id', $id_user))
        {
            return HttpResponse::response('Usuario no encontrado', 404);
        }

        $paciente =  $this->pacienteRepository->find($id_user);
        return HttpResponse::response($paciente, 200);
    }
    public function buscarPaciente ($paciente_id)
    {
        if (!VerifyExistence::response('paciente', 'id_paciente', $paciente_id))
        {
            return HttpResponse::response('Paciente no encontrado', 404);
        }

        $paciente =  $this->pacienteRepository->buscarPaciente($paciente_id);
        return HttpResponse::response($paciente, 200);
    }


    public function buscarEnfermedadesPaciente ($paciente_id)
    {
        if (!VerifyExistence::response('paciente', 'id_paciente', $paciente_id))
        {
            return HttpResponse::response('Paciente no encontrado', 404);
        }

        $paciente =  $this->pacienteRepository->buscarEnfermedadesPaciente($paciente_id);
        return HttpResponse::response($paciente, 200);
    }
    

    public function buscarMedicamentosPaciente ($paciente_id)
    {
        if (!VerifyExistence::response('paciente', 'id_paciente', $paciente_id))
        {
            return HttpResponse::response('Paciente no encontrado', 404);
        }

        $paciente =  $this->pacienteRepository->buscarMedicamentosPaciente($paciente_id);
        return HttpResponse::response($paciente, 200);
    }

    public function listarColumnas ()
    {       
        $columnas=[];
        $sexo =  $this->pacienteRepository->listarColumnas("sexo");
        array_push($columnas,$sexo);
        $grupoSanguineo =  $this->pacienteRepository->listarColumnas("grupo_sanguineo");
        array_push($columnas,$grupoSanguineo);
        $sistemaSalud =  $this->pacienteRepository->listarColumnas("sistema_salud");
        array_push($columnas,$sistemaSalud);
        return HttpResponse::response($columnas, 200);
    }
}