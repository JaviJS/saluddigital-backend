<?php

namespace App\Services;

use App\Helpers\HttpResponse;
use App\Helpers\VerifyExistence;
use App\Repositories\CitaMedica\CitaMedicaRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use \Datetime;
use \DateInterval;
use \DatePeriod;
use Illuminate\Support\Facades\Storage;
class CitaMedicaService
{

    private $citaMedicaRepository;

    public function __construct(CitaMedicaRepositoryInterface $citaMedicaRepository)
    {
        $this->citaMedicaRepository = $citaMedicaRepository;
    }


    public function all()
    {
        $citasMedicas = $this->citaMedicaRepository->all();
        return HttpResponse::response($citasMedicas, 200);
    }

    public function create (Request $request)
    {

        try {

           
            $citaMedica = $request->all();
            $citaMedica['estado'] = 'Pendiente';
            $citaMedica['created_at'] = Carbon::now('America/Santiago');
            $status = $this->citaMedicaRepository->create($citaMedica);

            if (!$status)
            {
                return HttpResponse::response('Error al crear la cita médica', 500);
            }

            return HttpResponse::response($status, 201);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }

    }


    public function update (Request $request, $cita_medica_id)
    {
        
        try {

            if (!VerifyExistence::response('cita_medica', 'id_cita_medica', $cita_medica_id))
            {
                return HttpResponse::response('Cita Medica no encontrado', 404);
            }

            $citaMedica  = $request->all();
            $citaMedica['updated_at'] = Carbon::now('America/Santiago');

            $status = $this->citaMedicaRepository->update($citaMedica, $cita_medica_id);

            if (!$status)
            {
                return HttpResponse::response('Error al actualizar la citaMedica', 500);
            }

            $citasMedicas = $this->citaMedicaRepository->all();

            return HttpResponse::response($citasMedicas, 200);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }


    }

    public function delete ($cita_medica_id)
    {

        if (!VerifyExistence::response('cita_medica', 'id_cita_medica', $cita_medica_id))
        {
            return HttpResponse::response('Cita Medica no encontrada', 404);
        }

        $status = $this->citaMedicaRepository->delete($cita_medica_id);

        if (!$status)
        {
            return HttpResponse::response('Error al eliminar la cita medica', 500);
        }

        $citasMedicas = $this->citaMedicaRepository->all();

        return HttpResponse::response($citasMedicas, 200);        
    }


    public function find ($cita_medica_id)
    {
        if (!VerifyExistence::response('cita_medica', 'id_cita_medica', $cita_medica_id))
        {
            return HttpResponse::response('Cita Medica no encontrado', 404);
        }

        $citaMedica =  $this->citaMedicaRepository->find($cita_medica_id);
        return HttpResponse::response($citaMedica, 200);
    }

    public function citasMedicasPaciente($paciente_id)
    {
        $citasMedicas = $this->citaMedicaRepository->citasMedicasPaciente($paciente_id);
        return HttpResponse::response($citasMedicas, 200);
    }

    public function citasMedicasPendientesPaciente($paciente_id)
    {
        $citasMedicas = $this->citaMedicaRepository->citasMedicasPendientesPaciente($paciente_id);
        return HttpResponse::response($citasMedicas, 200);
    }

    public function citasMedicasFinalizadasPaciente($paciente_id)
    {
        $citasMedicas = $this->citaMedicaRepository->citasMedicasFinalizadasPaciente($paciente_id);
        return HttpResponse::response($citasMedicas, 200);
    }
    
    public function agregarObservaciónCita (Request $request, $cita_medica_id)
    {
        
        try {

            if (!VerifyExistence::response('cita_medica', 'id_cita_medica', $cita_medica_id))
            {
                return HttpResponse::response('Cita Medica no encontrado', 404);
            }
            
            $citaMedica  = $request->all();            
            $citaMedica['estado'] = 'Finalizada';
            $citaMedica['updated_at'] = Carbon::now('America/Santiago');

            
            $status = $this->citaMedicaRepository->update($citaMedica, $cita_medica_id);
            
            if (!$status)
            {
                return HttpResponse::response('Error al actualizar la cita medica', 500);
            }

           

            return HttpResponse::response($status, 200);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }
    }

    public function cancelarCita (Request $request, $cita_medica_id)
    {
        
        try {

            if (!VerifyExistence::response('cita_medica', 'id_cita_medica', $cita_medica_id))
            {
                return HttpResponse::response('Cita Medica no encontrado', 404);
            }
            
            $citaMedica  = $request->all();            
            $citaMedica['estado'] = 'Cancelada';
            $citaMedica['updated_at'] = Carbon::now('America/Santiago');

            
            $status = $this->citaMedicaRepository->update($citaMedica, $cita_medica_id);
            
            if (!$status)
            {
                return HttpResponse::response('Error al actualizar la cita medica', 500);
            }

            $citasMedicas = $this->citaMedicaRepository->all();

            return HttpResponse::response($citasMedicas, 200);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }
    }


    public function reasignarCita(Request $request, $cita_medica_id)
    {
        
        try {

            if (!VerifyExistence::response('cita_medica', 'id_cita_medica', $cita_medica_id))
            {
                return HttpResponse::response('Cita Medica no encontrado', 404);
            }
            
            $citaMedica  = $request->all();            
            $citaMedica['estado'] = 'Pendiente';
            $citaMedica['updated_at'] = Carbon::now('America/Santiago');

            
           $status = $this->citaMedicaRepository->update($citaMedica, $cita_medica_id);
            
            if (!$status)
            {
                return HttpResponse::response('Error al actualizar la cita medica', 500);
            }


            return HttpResponse::response($status, 200);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }
    }


    public function buscarFechaCitaMedica (Request $request)
    {
        
        try {
            $citaMedica  = $request->all(); 
            if (!VerifyExistence::response('especialidad_cesfam', 'id_especialidad_cesfam', $citaMedica['id_especialidad_cesfam']))
            {
                return HttpResponse::response('Especialidad cesfam no encontrado', 404);
            }
            
                      
            $citaMedica['fecha']= Carbon::now('America/Santiago');
            $cupo = $this->citaMedicaRepository->buscarCupoEspecialidad($citaMedica['id_especialidad_cesfam']);
            $citaMedica = array_merge($citaMedica,$cupo[0]);

            $fechas= array_column($this->citaMedicaRepository->buscarFechaCitaMedica($citaMedica), 'fecha');
            $status['citas_canceladas']=$this->citaMedicaRepository->buscarCitasCanceladas($citaMedica);
            
            
            $fechas_llenas=array();
           foreach($fechas as $key => $cita) {
                $cita_sin_cupo['fecha']=$cita;
                $cita_sin_cupo['id_especialidad_cesfam']=$citaMedica['id_especialidad_cesfam'];
                $buscarFechas= $this->citaMedicaRepository->buscarFechasSinCupo($cita_sin_cupo); 
                if(count($buscarFechas)==$cupo[0]['cupo']){
                   array_push( $fechas_llenas,$cita);
                }  
            }
            $status['citas_sin_cupo']= $fechas_llenas;

            if (!$status)
            {
                return HttpResponse::response('Error al actualizar la cita medica', 500);
            }
            return HttpResponse::response($status, 200);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }
    }

    public function buscarHorasDisponibles (Request $request)
    {
        
        try {
            $datos  = $request->all(); 
            if (!VerifyExistence::response('paciente', 'id_paciente', $datos['id_paciente']))
            {
                return HttpResponse::response('Paciente no encontrado', 404);
            }
                                   
            $cupo = $this->citaMedicaRepository->buscarCupoEspecialidad($datos['id_especialidad_cesfam']);

            $fechasDisponible['id_especialidad_cesfam']=$datos['id_especialidad_cesfam'];
            $fecha= $datos['fecha'];
            $fechasDisponible['fecha']=[$fecha.' 00:00:00',$fecha.' 23:59:59'];
            
            $horasNoDisponibles = array_column($this->citaMedicaRepository->buscarHorasDisponibles($fechasDisponible),'fecha');

            //hora de inicio de la cita medica
            $hora_inicio_inicio = new DateTime( '08:00:00' );
            $hora_inicio_fin    = new DateTime( '16:00:00' );
            $hora_inicio_fin->modify('+1 second'); // Añadimos 1 segundo para que nos muestre $hora_fin
        
            $rango_hora= 9;  //horas
            $intervalo_horas = ($rango_hora / $cupo[0]['cupo']) * 60;

            if ($hora_inicio_inicio > $hora_inicio_fin) {
        
                $hora_inicio_fin->modify('+1 day');
            } 
            $intervalo = new DateInterval('PT'.$intervalo_horas.'M');
            $periodo   = new DatePeriod($hora_inicio_inicio, $intervalo, $hora_inicio_fin);     
       

            $horas=[];
            $array_horas=array();
            foreach($periodo as $key => $hora ) {
                //True, si existe una fecha, false si no coincide
                $disponibilidad =$this->citaMedicaRepository->disponibilidadCitaPaciente($datos['id_paciente'],$fecha.' '.$hora->format('H:i:s')); 
      
                if($disponibilidad==false){                 
                    $horas=array(
                        'hora_inicio' => $hora->format('H:i:s'),
                        'hora_final' =>  $hora->modify('+'.$intervalo_horas.' minutes')->format('H:i:s'),
                        'nro_espera' =>  $key+1
                    );
                    array_push($array_horas,$horas);
                }             
            }
         
            //$array = array_diff(array_column($array_horas, 'hora_inicio'), $horasDisponibles);
       

            $horitas=array_column($array_horas, 'hora_inicio');
            $hola= array();
     
            for ($i=0; $i < count($horitas); $i++) { 
                $resp=in_array($horitas[$i], $horasNoDisponibles);
                if($resp){
                    unset($array_horas[$i]);
                }                              
            }   
            return HttpResponse::response(array_values($array_horas), 200);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }
    }

    public function buscarFechasCitaParaPacientes (Request $request)
    {
        
        try {

            $datos  = $request->all(); 
            if (!VerifyExistence::response('especialidad_cesfam', 'id_especialidad_cesfam', $datos['id_especialidad_cesfam']))
            {
                return HttpResponse::response('Especialidad cesfam no encontrado', 404);
            }          

            $fecha = new Carbon($datos['fecha'],'America/Santiago'); //fecha que envio
            $fecha_inicial = new Carbon($datos['fecha'],'America/Santiago'); 
           
            $cupo = $this->citaMedicaRepository->buscarCupoEspecialidad($datos['id_especialidad_cesfam']);  //calcular cupo de una especialidad
            $datos = array_merge($datos,$cupo[0]);  //junta la especialidad, fecha y cupos de ese dia
            $citas= $this->citaMedicaRepository->buscarFechaCitaMedica($datos);  //fechas con nro de espera 9
            

            $prueba="";
            $citas_array = array_column($citas, 'fecha'); //transforma las fechas con nro de espera 9 en arrat

            //buscar si hay horas libres en esas fechas

            $fechas_llenas=array(); //creo un array para guardar las fechas con citas llenas
            // crear loop para recorrer  $citas_array
            foreach($citas_array as $key => $cita) {
                 $cita_sin_cupo['fecha']=$cita;
                 $cita_sin_cupo['id_especialidad_cesfam']=$datos['id_especialidad_cesfam'];
                 //buscar si fecha tiene cupo
                 $buscarFechas= $this->citaMedicaRepository->buscarFechasSinCupo($cita_sin_cupo); 
                 //validar si fecha tiene cupo y se agrrega a un nuevo array de fechas sin cuo
                 if(count($buscarFechas)==$cupo[0]['cupo']){
                    array_push($fechas_llenas,$cita);
                 }  
             }

            //si array esta vacio devuelve el dia de mañana       
                $condicion=true;
                //se hace hasta que encuentre la fecha con cupo
                while($condicion){                
                    //si la fecha entregada no existe en las fechas con cupo
                    if(!in_array($fecha->format('Y-m-d'),$fechas_llenas)){
                        //si la fecha entregada es igual a fecha inicial se suma un dia
                        if($fecha==$fecha_inicial && !in_array($fecha->format('Y-m-d'),$fechas_llenas)){
                            $prueba=$fecha->addDay(1)->format('Y-m-d');
                            
                        }else{ //si la fecha entregada no es igual a la fecha inicial se devuelve la fecha 
                            $prueba=$fecha->format('Y-m-d');
                            $condicion=false;
                        }
                    }else{//si la fecha entregada existe en las fechas con cupo, se devuelve el siguiente dia
                        $fecha=$fecha->addDay(1);
                    }                
                
            }
            return HttpResponse::response($prueba, 200);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }
    }


    public function agregarDocumentosCita(Request $request,$id)
    {

        try {
        if($request->hasFile('files')){
            foreach($request->file('files') as $file) {                
                
                $filename = $file->getClientOriginalName(); 
                $pathImagen=$file->storeAs('public/documentos', $filename);
                $urlImagen = Storage::url($pathImagen);
                $documento['ruta'] = $urlImagen;
                $documento['created_at'] = Carbon::now('America/Santiago'); 
                $documento['cita_medica_id'] = $id; 
                $status = $this->citaMedicaRepository->agregarDocumentosCitas($documento);
            }
        }else{
            return HttpResponse::response('Error al crear los documentos', 500);
         }    
            
            return HttpResponse::response("ok", 201);

        }catch (\Exception $exception)
        {
            return HttpResponse::response('Error :' . $exception->getMessage() , 400);
        }

    }



    public function verDocumentosCita($cita_medica_id)
    {
        if (!VerifyExistence::response('cita_medica', 'id_cita_medica', $cita_medica_id))
        {
            return HttpResponse::response('Cita Medica no encontrado', 404);
        }

        $documentos =  $this->citaMedicaRepository->verDocumentosCita($cita_medica_id);
        return HttpResponse::response($documentos, 200);
    }

}